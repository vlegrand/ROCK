.\" Automatically generated by Pod::Man 4.11 (Pod::Simple 3.35)
.\"
.\" Standard preamble:
.\" ========================================================================
.de Sp \" Vertical space (when we can't use .PP)
.if t .sp .5v
.if n .sp
..
.de Vb \" Begin verbatim text
.ft CW
.nf
.ne \\$1
..
.de Ve \" End verbatim text
.ft R
.fi
..
.\" Set up some character translations and predefined strings.  \*(-- will
.\" give an unbreakable dash, \*(PI will give pi, \*(L" will give a left
.\" double quote, and \*(R" will give a right double quote.  \*(C+ will
.\" give a nicer C++.  Capital omega is used to do unbreakable dashes and
.\" therefore won't be available.  \*(C` and \*(C' expand to `' in nroff,
.\" nothing in troff, for use with C<>.
.tr \(*W-
.ds C+ C\v'-.1v'\h'-1p'\s-2+\h'-1p'+\s0\v'.1v'\h'-1p'
.ie n \{\
.    ds -- \(*W-
.    ds PI pi
.    if (\n(.H=4u)&(1m=24u) .ds -- \(*W\h'-12u'\(*W\h'-12u'-\" diablo 10 pitch
.    if (\n(.H=4u)&(1m=20u) .ds -- \(*W\h'-12u'\(*W\h'-8u'-\"  diablo 12 pitch
.    ds L" ""
.    ds R" ""
.    ds C` ""
.    ds C' ""
'br\}
.el\{\
.    ds -- \|\(em\|
.    ds PI \(*p
.    ds L" ``
.    ds R" ''
.    ds C`
.    ds C'
'br\}
.\"
.\" Escape single quotes in literal strings from groff's Unicode transform.
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\"
.\" If the F register is >0, we'll generate index entries on stderr for
.\" titles (.TH), headers (.SH), subsections (.SS), items (.Ip), and index
.\" entries marked with X<> in POD.  Of course, you'll have to process the
.\" output yourself in some meaningful fashion.
.\"
.\" Avoid warning from groff about undefined register 'F'.
.de IX
..
.nr rF 0
.if \n(.g .if rF .nr rF 1
.if (\n(rF:(\n(.g==0)) \{\
.    if \nF \{\
.        de IX
.        tm Index:\\$1\t\\n%\t"\\$2"
..
.        if !\nF==2 \{\
.            nr % 0
.            nr F 2
.        \}
.    \}
.\}
.rr rF
.\"
.\" Accent mark definitions (@(#)ms.acc 1.5 88/02/08 SMI; from UCB 4.2).
.\" Fear.  Run.  Save yourself.  No user-serviceable parts.
.    \" fudge factors for nroff and troff
.if n \{\
.    ds #H 0
.    ds #V .8m
.    ds #F .3m
.    ds #[ \f1
.    ds #] \fP
.\}
.if t \{\
.    ds #H ((1u-(\\\\n(.fu%2u))*.13m)
.    ds #V .6m
.    ds #F 0
.    ds #[ \&
.    ds #] \&
.\}
.    \" simple accents for nroff and troff
.if n \{\
.    ds ' \&
.    ds ` \&
.    ds ^ \&
.    ds , \&
.    ds ~ ~
.    ds /
.\}
.if t \{\
.    ds ' \\k:\h'-(\\n(.wu*8/10-\*(#H)'\'\h"|\\n:u"
.    ds ` \\k:\h'-(\\n(.wu*8/10-\*(#H)'\`\h'|\\n:u'
.    ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'^\h'|\\n:u'
.    ds , \\k:\h'-(\\n(.wu*8/10)',\h'|\\n:u'
.    ds ~ \\k:\h'-(\\n(.wu-\*(#H-.1m)'~\h'|\\n:u'
.    ds / \\k:\h'-(\\n(.wu*8/10-\*(#H)'\z\(sl\h'|\\n:u'
.\}
.    \" troff and (daisy-wheel) nroff accents
.ds : \\k:\h'-(\\n(.wu*8/10-\*(#H+.1m+\*(#F)'\v'-\*(#V'\z.\h'.2m+\*(#F'.\h'|\\n:u'\v'\*(#V'
.ds 8 \h'\*(#H'\(*b\h'-\*(#H'
.ds o \\k:\h'-(\\n(.wu+\w'\(de'u-\*(#H)/2u'\v'-.3n'\*(#[\z\(de\v'.3n'\h'|\\n:u'\*(#]
.ds d- \h'\*(#H'\(pd\h'-\w'~'u'\v'-.25m'\f2\(hy\fP\v'.25m'\h'-\*(#H'
.ds D- D\\k:\h'-\w'D'u'\v'-.11m'\z\(hy\v'.11m'\h'|\\n:u'
.ds th \*(#[\v'.3m'\s+1I\s-1\v'-.3m'\h'-(\w'I'u*2/3)'\s-1o\s+1\*(#]
.ds Th \*(#[\s+2I\s-2\h'-\w'I'u*3/5'\v'-.3m'o\v'.3m'\*(#]
.ds ae a\h'-(\w'a'u*4/10)'e
.ds Ae A\h'-(\w'A'u*4/10)'E
.    \" corrections for vroff
.if v .ds ~ \\k:\h'-(\\n(.wu*9/10-\*(#H)'\s-2\u~\d\s+2\h'|\\n:u'
.if v .ds ^ \\k:\h'-(\\n(.wu*10/11-\*(#H)'\v'-.4m'^\v'.4m'\h'|\\n:u'
.    \" for low resolution devices (crt and lpr)
.if \n(.H>23 .if \n(.V>19 \
\{\
.    ds : e
.    ds 8 ss
.    ds o a
.    ds d- d\h'-1'\(ga
.    ds D- D\h'-1'\(hy
.    ds th \o'bp'
.    ds Th \o'LP'
.    ds ae ae
.    ds Ae AE
.\}
.rm #[ #] #H #V #F C
.\" ========================================================================
.\"
.IX Title "ROCK 1"
.TH ROCK 1 "2022-01-19" "Unix" "User Manuals"
.\" For nroff, turn off justification.  Always turn off hyphenation; it makes
.\" way too many mistakes in technical documents.
.if n .ad l
.nh
.SH "NAME"
.IP "\fBrock\fR \- Reducing Over Covering Kmers" 4
.IX Item "rock - Reducing Over Covering Kmers"
.SH "SYNOPSIS"
.IX Header "SYNOPSIS"
.PD 0
.IP "\fBrock\fR [\fB\-h\fR] [\fB\-i\fR \fIfile\fR] [\fB\-o\fR \fIfile\fR] [\fB\-k\fR \fIk_mer_size\fR]  [\fB\-q\fR \fInucl_qual_score_threshold\fR] [\fB\-C\fR \fIkappa\fR] [\fB\-c\fR \fIkappa_prime\fR] [\fB\-l\fR \fIlambda\fR] [\fB\-n\fR \fInb_distinct_k_mer\fR] [\fB\-m\fR \fImin_valid_k_mer_per_read\fR] [Args]" 4
.IX Item "rock [-h] [-i file] [-o file] [-k k_mer_size] [-q nucl_qual_score_threshold] [-C kappa] [-c kappa_prime] [-l lambda] [-n nb_distinct_k_mer] [-m min_valid_k_mer_per_read] [Args]"
.PD
.SH "OPTIONS"
.IX Header "OPTIONS"
Following command line options are allowed:
.IP "\-i \fIfile\fR" 4
.IX Item "-i file"
List of input fastq file names is in \fIfile\fR.
Rock can't handle more than 15 fastq files in input for technical reasons.
For more information on \fIfile\fR format, see the description section.
.IP "\-o \fIfile\fR List of output fastq file names is in \fIfile\fR. Note that names of output fastq files must be in the same order as names of input fastq. Result of filtering 1rst fastq file will go in the first fastq file mentionned in \fIfile\fR For more information on \fIfile\fR format, see the description section." 4
.IX Item "-o file List of output fastq file names is in file. Note that names of output fastq files must be in the same order as names of input fastq. Result of filtering 1rst fastq file will go in the first fastq file mentionned in file For more information on file format, see the description section."
.PD 0
.IP "\-k" 4
.IX Item "-k"
.PD
Specify wanted k\-mer size. Default is 25; maximum is 32.
.IP "\-q" 4
.IX Item "-q"
Specify minimum threshold for nucleotides quality score. Nucleotides that have a score below that threshold will be considered as errors (just like N nucleotides).
Default is 0.
.IP "\-m" 4
.IX Item "-m"
Can be used together with \-q.
Specify minimum number of correct k\-mers required to keep a read for \s-1CMS\s0 filter.
Indicate only a integer in version 1.4 and later.
.IP "\-C" 4
.IX Item "-C"
Specify maximum coverage wanted. Default is 70. Maximum is 65535.
.IP "\-c" 4
.IX Item "-c"
Specify lower threshold for coverage. Default is 0 (no lower threshold).
.IP "\-f" 4
.IX Item "-f"
Specify maximum collision probability in the \s-1CMS\s0 (default is 0.05). 
To be used with \-n so that \s-1ROCK\s0 can compute the appropriate number of arrays in the \s-1CMS.\s0
.IP "\-l" 4
.IX Item "-l"
Specify lambda (number of arrays wanted in the count min sketch filter).
Default is minimum 4.
.IP "\-n" 4
.IX Item "-n"
Indicate the number of distinct k\-mers in input fastq files. 
This is useful to compute a more appropriate value than the default one for lambda if you have not specified it with \-l.
.IP "\-v" 4
.IX Item "-v"
verbose mode. At the end of execution displays a recap of all \s-1ROCK\s0 parameters (name of input files, values of, kappa and happa_prime) plus, \s-1CMS\s0 size in Bytes, plus if \-n was provided, the expected probability of collision.
.IP "\-h" 4
.IX Item "-h"
Usage display.
.SH "DESCRIPTION"
.IX Header "DESCRIPTION"
\&\fB\s-1ROCK\s0\fR is a filter of fastq records. 
Its aim is to filter fastq files on different criteria:
    1rst the quality score: records with the highest quality score are processed first and thus are sure to be selected.
    2nd median coverage of the read: If more than half of the k\-mers in the read have occurred less than kappa times; then the read is selected.
    3rd median coverage of the read: if more than half of the k\-mers in the read have occurred less than kappa_prime times; then the read is removed.
    This is very useful for removing reads from \*(L"polluted\*(R" samples.
.PP
\&\fB\s-1ROCK\s0\fR is highly parameterizable via its options.
It is possible to refine filtering by adding more criteria:
    1) nucleotide score threshold: k\-mers containing at least 1 nucleotide below specified threshold will not be taken into account.
    2) minimum number of correct k\-mers : let X be this number. Reads containing less than X k\-mers without nucleotides below threshold will not be processed.
    They will be put in files named with .undefined extension.
    3) Pair end reads are processed as single.
.PP
It is possible to specify the size of the count min sketch by:
    1) indicating the number of arrays that you want in it (via the \-l option),
    2) indicating the number of different k\-mers in your input files (via the \-n option). Thus \fB\s-1ROCK\s0\fR will compute the best lambda value by minimizing the probability of collision and taking into account the amount of \s-1RAM\s0 on your system.
.PP
\&\fB\s-1ROCK\s0\fR supports 2 implementations for the count min sketch component. It chooses the implementation to use depending on kappa value. 
Indeed for small kappa values (ie <255) a single Byte can be used for storing k\-mer occurrences counters. 
This results in using less memory and faster execution.
For kappa values between 255 and 65535, 2 Bytes are used for storing k\-mer occurrences counters.
.SH "EXAMPLES"
.IX Header "EXAMPLES"
.IP "rock \-l 4 \-C 40 \-c 20 \-i input_souris.txt \-o output_souris.txt" 4
.IX Item "rock -l 4 -C 40 -c 20 -i input_souris.txt -o output_souris.txt"
Rock will filter the files listed in input_souris.txt and write the filtered fastq in the files mentioned in output_souris.txt.
To perform the filtering it will use a byte implementation (since kappa=40 which is smaller than 255) of the count min sketch that
will contains 4 arrays (lambda=4) of counters.
Reads for median coverage < 20 (kappa_prime) will be removed.
.IP "rock my_PE1.fq,myPE2.fq my_single.fq" 4
.IX Item "rock my_PE1.fq,myPE2.fq my_single.fq"
Rock will filter the 3 files in input  with default parameters:
high filter=70, 
low filter=0
nucleotide quality score threshold=0
minimum number of correct k\-mer in a read=1
lambda=4.
.Sp
And it will produce 3 output files in the execution directory.
These files will be named:
my_PE1.rock.fq,myPE2.rock.fq 
my_single.rock.fq
.SH "NOTE ON MEMORY CONSUMPTION."
.IX Header "NOTE ON MEMORY CONSUMPTION."
count min sketch size is MAX_UINT*lambda or MAX_UINT*lambda*2 if kappa>255.
Besides count min sketch, memory is also needed to store information related to reads in the input fastq files. 
13 Bytes per fastq record are needed plus some memory for the containers to work and for the read/write buffers.
.PP
To give you an idea, processing approximately 1.8 billion of reads in 2 fastq files (tot 500 \s-1GB\s0 data on disk) took 28 \s-1GB\s0 of memory plus 16 \s-1GB\s0 for the \s-1CMS\s0 (lambda=4, byte implementation).
.SH "AUTHOR"
.IX Header "AUTHOR"
Alexis Criscuolo (criscuol@pasteur.fr), Veronique Legrand (vlegrand@pasteur.fr) Institut Pasteur.
