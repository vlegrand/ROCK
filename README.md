[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![DOI](https://joss.theoj.org/papers/10.21105/joss.03790/status.svg)](https://doi.org/10.21105/joss.03790)
[![poster](https://img.shields.io/badge/doi-10.14293/S2199--1006.1.SOR--.PPNAZX5.v1-brown.svg)](https://doi.org/10.14293/S2199-1006.1.SOR-.PPNAZX5.v1)


# ROCK

_ROCK_ (Reducing Over-Covering K-mers) is a command line program written in [C++](https://isocpp.org/) that runs an alternative implementation of the _digital normalization_ method (e.g. Brown et al. 2012, Wedemeyer et al. 2017, Durai and Schulz 2019).

Given one or several [FASTQ](https://en.wikipedia.org/wiki/FASTQ_format) file(s), the main aim of _ROCK_ is to build a subset of accurate high-throughput sequencing (HTS) reads such that the induced coverage depth is comprised between two specified lower- and upper-bounds. 
_ROCK_ can therefore be used to reduce and/or homogenize the overall coverage depth within large sets of HTS reads, which is often required to quickly infer accurate _de novo_ genome assemblies (e.g. Desai et al. 2013, Chen et al. 2015).
_ROCK_ can also be used to discard low-covering HTS reads, as those ones are often artefactual, highly erroneous or contaminating sequences. 

For more details, see the associated publications: Legrand et al. (2022a, 2022b).


## Compilation and installation

#### Prerequisites

First and foremost, clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/vlegrand/ROCK.git
```

_ROCK_ is developped in C++ 98 and compatible with C++ 11.
Compilation of the source code can be carried out using [gcc](https://ftp.gnu.org/gnu/gcc/) (version &ge; 4.4.7) or [clang](https://clang.llvm.org/) (version &ge; 503.0.40), together with the tools [_make_](https://www.gnu.org/software/make), [_autoconf_](https://www.gnu.org/software/autoconf) (version 2.69) and [_automake_](https://www.gnu.org/software/automake) (version 1.16).

#### Basic compilation and execution

An executable can be built by running the five following command lines:

```bash
autoconf
automake
./configure
make check
make
```

The first command line generates the files required by [_make_](https://www.gnu.org/software/make) to compile the source code. 
<br> 
The second command line (non-mandatory) compiles the source code, and next runs all unit and non-regression tests. 
If a problem occurs during this step, please report it by creating an issue in https://gitlab.pasteur.fr/vlegrand/ROCK.
<br>
The third command line finally compiles the source code to built the executable _src/rock_.

The executable _rock_  can be run with the following command line model:

```bash
cd src
./rock [options]
```

#### Advanced compilation and installation

Better performances can be obtained by passing the `-O3` flag during the configuration step:

```bash
./configure  CFLAGS='-O3'  CXXFLAGS='-O3'
```

You can also specify a location for the built executable, e.g.

```bash
./configure  --prefix=$path_to_output_directory
```

Otherwise, the executable _rock_ can be installed in the system default location (e.g. _/usr/local/_) using the following fourth command line (after the three ones described above): 

```bash
make install
```

Note that this last command line can require some extra permissions (e.g. `sudo`).


#### Installation in a mamba virtual environment

It is possible to install the autotools and to compile _ROCK_ in a [mamba](https://mamba.readthedocs.io/en/latest) virtual environment using to following command lines:


```bash
mamba create -n ROCK_env
mamba init 
cd ROCK/
mamba activate ROCK_env
mamba install -y autoconf
mamba install -y automake
autoconf
aclocal
automake
./configure
make check
```


## Usage

Run _ROCK_ without option to read the following documentation:

```
Reducing Over-Covering K-mers within FASTQ file(s)

USAGE: rock [options] [files]

OPTIONS:
 -i <file>  file containing the name(s)  of the input FASTQ file(s) to
            process;  single-end: one file name per line;  paired-end:
            two file names  per line  separated by  a comma;  up to 15
            FASTQ file  names can  be specified;  of note,  input file
            name(s) can also be specified as program argument(s)
 -o <file>  file containing the  name(s) of the  output FASTQ file(s);
            FASTQ file name(s) should be structured in the same way as
            the file specified in option -i.
 -k <int>   k-mer length (default 25)
 -c <int>   lower-bound k-mer coverage depth threshold (default: 0)
 -C <int>   upper-bound k-mer coverage depth threshold (default: 70)
 -l <int>   number of hashing function(s) (default: 4)
 -n <int>   expected total number of  distinct k-mers within the input
            read sequences; not compatible with option -l.
 -f <float> maximum expected false positive probability when computing
            the optimal number of hashing functions from the number of
            distinct k-mers specified with option -n (default: 0.05).
 -q <int>   sets as valid  only k-mers  made  up  of  nucleotides with
            Phred score (+33 offset) above this cutoff (default: 0)
 -m <int>   minimum  number  of valid  k-mer(s)  to  consider  a  read 
            (default: 1)
 -v         verbose mode
 -h         prints this message and exit
```


## Notes

* In brief, given an upper-bound _k_-mer coverage depth cutoff <em>&kappa;</em> (option `-C`) and a lower-bound _k_-mer coverage cutoff <em>&kappa;'</em> (option `-c`), the aim of _ROCK_ is to select an HTS read subset _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub> such that the overall _k_-mer coverage depth induced by the members of _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub> is expected to be always comprised between <em>&kappa;'</em> and <em>&kappa;</em>. When considering FASTQ files with high redundancy (i.e. coverage depth greater than <em>&kappa;</em>), _ROCK_ therefore returns smaller FASTQ files such that each of its HTS read corresponds to a genome region with _k_-mer coverage depth of at most <em>&kappa;</em>. Setting  <em>&kappa;'</em> (option `-c`) enables to discard HTS reads associated to a _k_-mer coverage depth lower than this lower-bound cutoff, which is often observed with artefactual, highly erroneous or contaminating HTS reads.

* After creating an empty count-min sketch (CMS; see below) to store the number of occurrences of every canonical _k_-mer shared by the input HTS reads, _ROCK_ proceeds in three main steps :
  1. Sorting the input SE/PE HTS reads from the most to the less accurate ones (as defined by the sum of the Phred scores).
  2. For each sorted SE/PE HTS read(s), approximating its _k_-mer coverage depth <em>c</em><sub><em>k</em></sub> (defined by the median of its _k_-mer occurence values, as returned by the CMS); if <em>c</em><sub><em>k</em></sub>&nbsp;&le;&nbsp;<em>&kappa;</em>, then adding the SE/PE HTS read(s) into the subset _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub> and updating the CMS for every corresponding canonical _k_-mer.
  3. (when the lower-bound cutoff <em>&kappa;'</em>&nbsp;>&nbsp;0) For each SE/PE HTS read(s) in _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub>, (re)approximating its _k_-mer coverage depth <em>c</em><sub><em>k</em></sub>; if <em>c</em><sub><em>k</em></sub>&nbsp;&le;&nbsp;<em>&kappa;'</em>, then removing the SE/PE HTS read(s) from _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub>. <br>

  At the end, all SE/PE HTS read(s) inside _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub> are written into output FASTQ file(s) (by default, file extension _.rock.fastq_). It is worth noticing that the step 1 ensures that all the HTS reads inside the returned subset _S_<sub><em>&kappa;'</em>,<em>&kappa;</em></sub> are the most accurate ones.

* _ROCK_ stores the number of occurences of every traversed canonical _k_-mer in a count-min sketch (CMS; e.g. Cormode and Muthukrishnan 2005), a dedicated probabilistic data structure with controllable false positive probability (FPP). By default, _ROCK_ instantiates a CMS based on four hashing functions (option `-l`), which can be sufficient for many cases, e.g. up to 10 billions canonical _k_-mers with <em>&kappa;</em>&nbsp;>&nbsp;<em>&kappa;'</em>&nbsp;>&nbsp;1 and FPP&nbsp;&le;&nbsp;5%. However, as each hashing function is defined on [0,&nbsp;2<sup>32</sup>[, the memory footprint of the CMS is 4<em>&lambda;</em>&nbsp;Gb (when _&kappa;_&nbsp;&le;&nbsp;255, twice otherwise), where <em>&lambda;</em> is the total number of hashing functions. It is therefore highly recommanded to provide the expected number _F_<sub>0</sub> of canonical _k_-mers (option `-n`) to enable _ROCK_ to compute the optimal CMS dimension <em>&lambda;</em> required to store this specified number of canonical _k_-mers with low FPP (option `-f`), e.g. <em>&lambda;</em>&nbsp;=&nbsp;1 is sufficient to deal with up to 3 billions canonical _k_-mers when <em>&kappa;</em>&nbsp;>&nbsp;<em>&kappa;'</em>&nbsp;>&nbsp;1 while ensuring FPP&nbsp;&le;&nbsp;5%. Moreover, a CMS based on few hashing functions entails faster running times. For instance, the programs [_KMC_](https://github.com/refresh-bio/KMC) (Deorowicz et al. 2013, 2015; Kokot et al. 2017), [_KmerStream_](https://github.com/pmelsted/KmerStream) (Melsted and Halldórsson 2014) or [_ntCard_](https://github.com/bcgsc/ntCard) (Mohamadi et al. 2017) can be used to quickly approximate this number (_F_<sub>0</sub>). 

* Of important note is that each of the upper- and lower-bound cutoffs (options `-C` and `-c`, respectively) corresponds to a _k_-mer coverage depth value (denoted here <em>c</em><sub><em>k</em></sub>), which is quite different to the base coverage depth value (denoted here <em>c</em><sub><em>b</em></sub>). However, when _L_ is the average input HTS read length, <em>c</em><sub><em>b</em></sub>&nbsp;/&nbsp;<em>c</em><sub><em>k</em></sub> and _L_&nbsp;/&nbsp;(_L_&nbsp;&minus;&nbsp;_k_&nbsp;+&nbsp;1) are expected to be identical for any fixed small _k_ (e.g. Liu et al. 2013). In consequence, when an overall (base) coverage depth <em>c</em><sub><em>b</em></sub> is expected, one can therefore set <em>&kappa;</em> =  <em>c</em><sub><em>b</em></sub> (_L_&nbsp;&minus;&nbsp;_k_&nbsp;+&nbsp;1)&nbsp;/&nbsp;_L_. For example, when dealing with HTS reads of length _L_&nbsp;=&nbsp;144 (on average), an HTS read subset with expected base coverage depth <em>c</em><sub><em>b</em></sub>&nbsp;=&nbsp;60x can be inferred by _ROCK_ by setting _k_&nbsp;=&nbsp;25 (option `-k`) and <em>&kappa;</em>&nbsp;=&nbsp;60&nbsp;(144&nbsp;&minus;&nbsp;25&nbsp;+&nbsp;1)&nbsp;/&nbsp;144&nbsp;=&nbsp;50 (option `-C`).

* By default, _ROCK_ uses _k_-mers of length _k_&nbsp;=&nbsp;25 (option `-k`). Increasing this length is not recommanded when dealing with large FASTQ files (e.g. average coverage depth > 500x from genome size > 1 Gbps), as the total number of canonical _k_-mers can quickly grow, therefore implying a very large CMS (i.e. many hashing functions) to maintains low FPP (e.g. &le;&nbsp;0.05). Using small _k_-mers (e.g. _k_&nbsp;<&nbsp;21) is also not recommanded, as this can negatively affect the overall specificity (i.e. too many identical _k_-mers arising from different sequenced genome region).

* All _ROCK_ steps are based on the usage of valid _k_-mers, i.e. _k_-mers that do not contain any undetermined base `N`. Valid _k_-mers can also be determined by bases associated to a Phred score greater than a specified threshold (option `-q`; Phred +33 offset, default:&nbsp;0). A minimum number of valid _k_-mers can be specified to consider a SE/PE HTS read(s) (option `-m`; default:&nbsp;1). 


## Examples

##### Single FASTQ file

The following [Bash](https://www.gnu.org/software/bash/) command line enables to download and uncompress the FASTQ file (845 Mb) associated to the run accession [ERR6807819](https://www.ncbi.nlm.nih.gov/sra/ERR6807819):

```bash
wget -O - https://ftp.sra.ebi.ac.uk/vol1/fastq/ERR680/009/ERR6807819/ERR6807819.fastq.gz | zcat > ERR6807819.fastq
```

The downloaded FASTQ file _ERR6807819.fastq_ contains 2,310,531 Ion Torrent S5 reads (394,782,864 bases) corresponding to the whole genome sequencing of a SARS-CoV-2 strain.

_ROCK_ can be directly run on this FASTQ file to perform digital normalization using default options (e.g. _k_&nbsp;=&nbsp;25, <em>&kappa;</em>&nbsp;=&nbsp;70, <em>&kappa;'</em>&nbsp;&nbsp;=&nbsp;0):

```bash
rock  ERR6807819.fastq
```

After performing the digital normalization of these single-end reads (running time: ~1 minute), _ROCK_ writes the default output FASTQ file _ERR6807819.rock.fastq_ (93,810 HTS reads, 13,467,261 bases).

The default values of the upper- and lower-bound _k_-mer coverage depth cutoff  <em>&kappa;</em> and <em>&kappa;'</em> can be modified using options `-C` and `-c`, respectively:

```bash
rock  -c 5  -C 80  ERR6807819.fastq
```

The above command line leads to 76,523 HTS reads (11,608,269 bases).


As [ERR6807819](https://www.ncbi.nlm.nih.gov/sra/ERR6807819) is the result of the sequencing of a SARS-CoV-2 genome, it is expected that its number _F_<sub>0</sub> of distinct canonical _k_-mers is far below 3 billions. In consequence, _ROCK_ can be adequately run using only <em>&lambda;</em>&nbsp;=&nbsp;1 hashing function:

```bash
rock  -c 5  -C 80  -l 1  ERR6807819.fastq
```

The above command line leads to 76,526 HTS reads (11,607,360 bases).

Alternatively, _F_<sub>0</sub> can be priorly estimated (e.g. with _k_&nbsp;=&nbsp;25, [_ntCard_](https://github.com/bcgsc/ntCard) returns _F_<sub>0</sub>&nbsp;=&nbsp;4,840,553), and next specified using option `-n`:

```bash
rock  -c 5  -C 80  -n 4840553  -v  ERR6807819.fastq
```

As the option `-v` has been set, the above command line prints the following information:

```
SE input file(s)                        ERR6807819.fastq
upper-bound coverage depth              80
lower-bound coverage depth              5
k-mer length                            25
expected no. distinct k-mers            4840553
no. hash. function(s)                   1
expected false positive proba.          0.000000 (cov. depth > 1)
no. buckets for hash. 1                 4283781797
no. bits per bucket                     8
count-min sketch size (Gb)              4
no. input reads                         2310531
no. input k-mers                        339330120
no. unset buckets for hash. 1           4280838158
approx. no. distinct k-mers             2944656
approx. false positive proba.           0.000000
no. selected reads                      76526
SE output files                         ERR6807819.rock.fastq
```

This shows that <em>&lambda;</em>&nbsp;=&nbsp;1 hashing function is indeed sufficient to deal with _F_<sub>0</sub>&nbsp;=&nbsp;4,840,553 distinct canonical _k_-mers. The selected HTS read subset is therefore identical to the one obtained using `-l 1`.

Valid _k_-mers (to fill the CMS) can also be assessed by specifying a minimum Phred score using option `-q`, e.g. _Q_&nbsp;&geq;&nbsp;10:

```bash
rock  -c 5  -C 80  -n 4840553  -q 10  ERR6807819.fastq
```

The above command line leads to 44,687 HTS reads (7,367,710 bases).


##### Paired FASTQ files


The following [Bash](https://www.gnu.org/software/bash/) command lines enable to download and uncompress the two FASTQ files (199 Mb each) associated to the run accession [ERR7756257](https://www.ncbi.nlm.nih.gov/sra/ERR7756257):

```bash
EBIURL="https://ftp.sra.ebi.ac.uk/vol1/fastq";
wget -O - $EBIURL/ERR775/007/ERR7756257/ERR7756257_1.fastq.gz | gunzip -c > ERR7756257.1.fastq ;
wget -O - $EBIURL/ERR775/007/ERR7756257/ERR7756257_2.fastq.gz | gunzip -c > ERR7756257.2.fastq ;
```

The downloaded FASTQ files _ERR7756257.1.fastq_ and _ERR7756257.2.fastq_ contains 571,893 Illumina MiSeq PE reads (84,146,312 and  84,156,299 bases, respectively) corresponding to the whole genome sequencing of a SARS-CoV-2 strain.

_ROCK_ can be directly run on these paired FASTQ files to perform digital normalization using default options (e.g. _k_&nbsp;=&nbsp;25, <em>&kappa;</em>&nbsp;=&nbsp;70, <em>&kappa;'</em>&nbsp;&nbsp;=&nbsp;0):

```bash
rock  ERR7756257.1.fastq,ERR7756257.2.fastq
```

After performing the digital normalization of these HTS PE reads (running time: ~30 seconds), _ROCK_ writes the default output FASTQ files _ERR7756257.1.rock.fastq_ and  _ERR7756257.2.rock.fastq_ (17,984 HTS PE reads, 2,633,631 and 2,636,701 bases, respectively).

Input and output FASTQ file names can be specified into txt files, e.g.

```bash
echo "ERR7756257.1.fastq,ERR7756257.2.fastq" > infiles.txt ;
echo "ERR7756257.norm.R1.fastq,ERR7756257.norm.R2.fastq" > outfiles.txt ;
rock  -i infiles.txt  -o outfiles.txt
```

The above command lines lead to the paired FASTQ output files _ERR7756257.norm.R1.fastq_ and _ERR7756257.norm.R2.fastq_. PE FASTQ files should be specified in the same line, separated by a comma (`,`), without blank space. Up to 15 FASTQ files (paired or not) can be specified. Of important note, output FASTQ files should be specified in the same way and in the same order as the corresponding input FASTQ files.


Options described above (see Single FASTQ file) can also be used with paired FASTQ files:

```bash
rock  -c 5  -C 80  -n 2277975  -q 10  -v  -i infiles.txt  -o outfiles.txt
```

The above command line prints the following information:

```
PE input files                          ERR7756257.1.fastq ERR7756257.2.fastq
upper-bound coverage depth              80
lower-bound coverage depth              5
k-mer length                            25
expected no. distinct k-mers            2277975
no. hash. function(s)                   1
expected false positive proba.          0.000000 (cov. depth > 1)
no. buckets for hash. 1                 4283781797
no. bits per bucket                     8
count-min sketch size (Gb)              4
no. input reads                         571792
no. input k-mers                        140851747
no. unset buckets for hash. 1           4282925434
approx. no. distinct k-mers             856450
approx. false positive proba.           0.000000
no. selected reads                      15804
PE output files                         ERR7756257.norm.R1.fastq ERR7756257.norm.R2.fastq
```

The output FASTQ files contain 15,804 HTS PE reads (2,319,355 and 2,319,367 bases, respectively)



## References

Brown CT, Howe A, Zhang Q, Pyrkosz AB, Brom YH (2012) _A Reference-Free Algorithm for Computational Normalization of Shotgun Sequencing Data_. **arXiv**:[1203.4802v2](https://arxiv.org/abs/1203.4802v2).

Chen TW, Gan RC, Chang YF, Liao W-C, Wu TH, Lee C-C, Huang P-J, Lee C-Y, Chen Y-YM, Chiu CH, Tang P (2015) _Is the whole greater than the sum of its parts? De novo assembly strategies for bacterial genomes based on paired-end sequencing_. **BMC Genomics**, 16:648. [doi:10.1186/s12864-015-1859-8](https://doi.org/10.1186/s12864-015-1859-8).

Cormode G, Muthukrishnan S (2005) _An Improved Data Stream Summary: The Count-Min Sketch and its Applications_. **Journal of Algorithms**, 55:29-38. [doi:10.1016/j.jalgor.2003.12.001](https://doi.org/10.1016/j.jalgor.2003.12.001).

Deorowicz S, Debudaj-Grabysz A, Grabowski S (2013) _Disk-based k-mer counting on a PC_. **BMC Bioinformatics**, 14:160. [doi:10.1186/1471-2105-14-160](https://doi.org/10.1186/1471-2105-14-160).

Deorowicz S, Kokot M, Grabowski S, Debudaj-Grabysz A (2015) _KMC 2: Fast and resource-frugal k-mer counting_. **Bioinformatics**, 31(10):1569-1576. [doi:10.1093/bioinformatics/btv022](https://doi.org/10.1093/bioinformatics/btv022).

Desai A, Marwah VS, Yadav A, Jha V, Dhaygude K, Bangar U, Kulkarni V, Jere A (2013) _Identification of Optimum Sequencing Depth Especially for De Novo Genome Assembly of Small Genomes Using Next Generation Sequencing Data_. **PLoS ONE**, 8(4):e60204. [doi:10.1371/journal.pone.0060204](https://doi.org/10.1371/journal.pone.0060204).

Durai DA, Schulz MH (2019) _Improving in-silico normalization using read weights_. **Scientific Reports**, 9:5133. [doi:10.1038/s41598-019-41502-9](https://doi.org/10.1038/s41598-019-41502-9).

Kokot M, Długosz M, Deorowicz S (2017) _KMC 3: counting and manipulating k-mer statistics_. **Bioinformatics**, 33(17):2759-2761. [doi:10.1093/bioinformatics/btx304](https://doi.org/10.1093/bioinformatics/btx304).

Legrand V, Kergrohen T, Joly N, Criscuolo A (2022a) _ROCK: digital normalization of whole genome sequencing data_. **Journal of Open Source Software**, 7(73):3790. [doi:10.21105/joss.03790](https://doi.org/10.21105/joss.03790).

Legrand V, Kergrohen T, Joly N, Criscuolo A (2022b) _ROCK: digital normalization of whole genome sequencing data_. _In_: Lemaitre C, Becker E, Derrien T (eds), [**Proceedings of JOBIM 2022**](https://jobim2022.sciencesconf.org/data/pages/JOBIM2022_proceedings_posters_demos.pdf), Rennes, 5-8 July, 2022. P. 21. [doi:10.14293/S2199-1006.1.SOR-.PPNAZX5.v1](https://www.scienceopen.com/document/read?id=e3ce49e4-1817-447b-9b21-81c03d9458a6).

Liu B, Shi Y, Yuan J, Hu X, Zhang H, Li N, Li Z, Chen Y, Mu D, Fan W (2013) _Estimation of genomic characteristics by analyzing k-mer frequency in de novo genome projects_. **arXiv**:[1308.2012v2](https://arxiv.org/abs/1308.2012v2).

Melsted P, Halldórsson BV (2014) _KmerStream: streaming algorithms for k-mer abundance estimation_. **Bioinformatics**, 30(24):3541-3547. [doi:10.1093/bioinformatics/btu713](https://doi.org/10.1093/bioinformatics/btu713).

Mohamadi H, Khan H, Birol I (2017) _ntCard: a streaming algorithm for cardinality estimation in genomics data_. **Bioinformatics**, 33(9):1324-1330. [doi:10.1093/bioinformatics/btw832](https://doi.org/10.1093/bioinformatics/btw832).

Wedemeyer A, Kliemann L, Srivastav A, Schielke C, Reusch TB, Rosenstiel P (2017) _An improved filtering algorithm for big read datasets and its application to single-cell assembly_. **BMC Bioinformatics**, 18:324. [doi:10.1186/s12859-017-1724-7](https://doi.org/10.1186/s12859-017-1724-7).


## Contributions

Suggestions for improving _ROCK_ or for adding new functionalities, as well as merge requests, are welcome.


## Citations

Álvarez-Pérez S, Quevedo-Caraballo S, García ME, BlancoJL (2024)
_Prevalence and genetic diversity of azole-resistant Malassezia pachydermatis isolates from canine otitis and dermatitis: A 2-year study_.
**Medical Mycology**, 62:myae053.
[doi:10.1093/mmy/myae053](https://doi.org/10.1093/mmy/myae053)

Pottier M, Castagnet S, Gravey F, Leduc G, Sévin C, Petry S, Giard J-C, Le Hello S, Léon A (2022)
_Antimicrobial Resistance and Genetic Diversity of Pseudomonas aeruginosa Strains Isolated from Equine and Other Veterinary Samples_.
**Pathogens**, 12(1):64.
[doi:10.3390/pathogens12010064](https://doi.org/10.3390/pathogens12010064)
