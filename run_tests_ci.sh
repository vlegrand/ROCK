#!/bin/sh
source /etc/profile.d/gensoft_modules.sh || exit 1
module av || exit 2
module load gcc/9.2.0 || exit 3
module load m4/1.4.18 || exit 4
module load autoconf/2.69 || exit 5
module load automake/1.16.3 || exit 6
module list || exit 7
aclocal || exit 8
autoconf -i || exit 9
automake || exit 10
./configure || exit 11
make; cd src || exit 12
./unit_test_fqreader || exit 13
./unit_test_math_utils || exit 14
./unit_test_fqwriter || exit 15
./unit_test_cms || exit 16
./unit_test_read_utils || exit 17
cd ..; make check || exit 18
make distcheck || exit 19