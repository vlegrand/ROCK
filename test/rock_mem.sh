#!/bin/sh
[ -z "$srcdir" ] && srcdir="."
echo "srcdir"=${srcdir}

# Gather here the tests that require at least 4 GB of memory to run.
## do real work
## Test high filter
echo " "
echo "##################################################################################"
echo "testing high filter with PE processed separately"


../src/rock -C 100 -c 0 -l 2 -p -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt >/dev/null || exit 101

# output files should be the same size, contain the same elements but not in the same order.
nb_PE1=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_1.fq`
nb_PE2=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_2.fq`


[ -f "data/fastq.filtered/SRR1222430_1.filtered.fastq" ] || exit 102
[ -f "data/fastq.filtered/SRR1222430_2.filtered.fastq" ] || exit 103

nb_PE1_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_1.filtered.fastq`
nb_PE2_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_2.filtered.fastq`

test $nb_PE1 -eq $nb_PE1_filtered || exit 104
test $nb_PE2 -eq $nb_PE2_filtered || exit 105

echo "testing high filter with PE processed as single"
../src/rock -C 100 -c 0 -l 2  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt >/dev/null || exit 101

# output files should be the same size, contain the same elements but not in the same order.
nb_PE1=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_1.fq`
nb_PE2=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_2.fq`


[ -f "data/fastq.filtered/SRR1222430_1.filtered.fastq" ] || exit 106
[ -f "data/fastq.filtered/SRR1222430_2.filtered.fastq" ] || exit 107

nb_PE1_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_1.filtered.fastq`
nb_PE2_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_2.filtered.fastq`

test $nb_PE1 -eq $nb_PE1_filtered || exit 108
test $nb_PE2 -eq $nb_PE2_filtered || exit 109


echo "  "
echo "##################################################################################"
echo "testing high filter and -f option and PE processed separately"

../src/rock -C 100 -c 0 -l 1 -f 0.05 -p -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt >/dev/null || exit 110

# output files should be the same size, contain the same elements but not in the same order.
nb_PE1=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_1.fq`
nb_PE2=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_2.fq`



[ -f "data/fastq.filtered/SRR1222430_1.filtered.fastq" ] || exit 111
[ -f "data/fastq.filtered/SRR1222430_2.filtered.fastq" ] || exit 112

nb_PE1_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_1.filtered.fastq`
nb_PE2_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_2.filtered.fastq`

test $nb_PE1 -eq $nb_PE1_filtered || exit 112
test $nb_PE2 -eq $nb_PE2_filtered || exit 114

echo "testing high filter and -f option and PE processed as single"

../src/rock -C 100 -c 0 -l 1 -f 0.05  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt >/dev/null || exit 115

# output files should be the same size, contain the same elements but not in the same order.
nb_PE1=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_1.fq`
nb_PE2=`grep -c "@" ${srcdir}/data/fastq.raw/klebsiella_100_2.fq`


[ -f "data/fastq.filtered/SRR1222430_1.filtered.fastq" ] || exit 116
[ -f "data/fastq.filtered/SRR1222430_2.filtered.fastq" ] || exit 117

nb_PE1_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_1.filtered.fastq`
nb_PE2_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_2.filtered.fastq`

test $nb_PE1 -eq $nb_PE1_filtered || exit 118
test $nb_PE2 -eq $nb_PE2_filtered || exit 119


# test low filter.
echo " "
echo "##################################################################################"
echo "testing low filter with PE processed separately"
../src/rock -C 100 -c 99 -l 2 -p -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt >/dev/null || exit 120

[ -f "data/fastq.filtered/SRR1222430_1.filtered.fastq" ] || exit 121
[ -f "data/fastq.filtered/SRR1222430_2.filtered.fastq" ] || exit 122

nb_PE1_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_1.filtered.fastq`
nb_PE2_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_2.filtered.fastq`

test $nb_PE1_filtered -eq 0 || exit 123
test $nb_PE2_filtered -eq 0 || exit 124

echo "testing low filter with PE processed as single"
../src/rock -C 100 -c 99 -l 2  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt >/dev/null || exit 125

[ -f "data/fastq.filtered/SRR1222430_1.filtered.fastq" ] || exit 126
[ -f "data/fastq.filtered/SRR1222430_2.filtered.fastq" ] || exit 127

nb_PE1_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_1.filtered.fastq`
nb_PE2_filtered=`grep -c "@" data/fastq.filtered/SRR1222430_2.filtered.fastq`

test $nb_PE1_filtered -eq 0 || exit 128
test $nb_PE2_filtered -eq 0 || exit 129

# test that input fastq file names can be provided in command-line.
echo " "
echo "##################################################################################"
echo "testing that input fastq file names can be provided in command line."
../src/rock -C 100 -c 1 -l 2 -p ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq ${srcdir}/data/unit/test_single.fq ${srcdir}/data/unit/test_single2.fq >/dev/null || exit 130

[ -f "klebsiella_100_1.rock.fq" ] || exit 131
[ -f "klebsiella_100_2.rock.fq" ] || exit 132
[ -f "test_single.rock.fq" ] || exit 133
[ -f "test_single2.rock.fq" ] || exit 134



# checking that output files were sorted in decreasing order of quality score. For that expect to have SRR122430.1.1 as 1rst record in filtered file.
ret=`head -4 "klebsiella_100_1.rock.fq"|grep -c "SRR1222430.1.1"`
test $ret -eq 2 || exit 135

echo "erasing test result files"
rm -f "klebsiella_100_1.rock.fq" || exit 136
rm -f "klebsiella_100_2.rock.fq" || exit 137
rm -f "test_single.rock.fq"|| exit 138
rm -f "test_single2.rock.fq"|| exit 139



# test the -v option
echo " "
echo "##################################################################################"
echo "testing verbose mode"
../src/rock -C 100 -c 1 -l 2 -v ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq ${srcdir}/data/unit/test_single.fq ${srcdir}/data/unit/test_single2.fq|grep "count-min sketch size (Gb)" >/dev/null || exit 140

../src/rock -C 100 -c 1 -v -n 1000 ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq ${srcdir}/data/unit/test_single.fq ${srcdir}/data/unit/test_single2.fq|grep "expected false positive proba" >/dev/null || exit 141

echo "erasing test result files"
rm -f "klebsiella_100_1.rock.fq" || exit 142
rm -f "klebsiella_100_2.rock.fq" || exit 143
rm -f "test_single.rock.fq"|| exit 144
rm -f "test_single2.rock.fq"|| exit 145


echo " "
echo "##################################################################################"
echo "testing rock with a quality score threshold for nucleotides and PE processed separately."

expected_diff1="304a305,308 \
  > @SRR1222430.37 37 length=250 \
> GCCTTTTCTTTTTCCAGGGAAAACCATCCAGGAGGAACTTTATTATGGCGATGTATGAAGTCGGTACCGTCACGGGTGCCGCGTCGCAGGCACGGGTGACAGGTGCGACAACAAAATGGTCACAGGAGGCGCTGGGGATACAGCCCGGGTCGATTCTGGTGGTCTACCGCAGCGGTAGTGCTGACCTGTATGCGATCAAATCCGTGGACAGCGACACGCAACTGACGCTGACCCGGAATATCACCACCGC \
> +SRR1222430.37 37 length=250 \
> AAAABFFFFFFFGCGFGGEFFCHHEEFHHGHHAFEAGAGGGFGGFHGFHGEEEFF5BDHHBDEGCEEHGAEGGFFGFCFHCGGC@E/EEEEHFEC@BBFFFG/GFEFDDCDHHGCC<?FGDDGHFFFGFFCGGG-.AGDGBGGHHGGG?ACGCFGG99B9;?C99B0CDGGCFF;DDFFFFFFFB99:;BBF/A.:9DFFF/F?EEFFFFFF>ADAFDFFFFFFE/ADADDFFFFFA;EEFF/BFFEEFF"

expected_diff2="> @SRR1222430.37 37 length=251 \
> GCTGGCCAGCTGGTTAGCAAACGACGAGGTGCTGGCGGGTTTAGCGGGAAAAATTGCGGAAGAGGCGCCGGGAAAAGCGGGGGTGTTATTACAGGTCTACGGCAGATGCGTGTCGCTGGCCGCGGCTTTTAGCGCCTACAAGTCTGGACTTCCCCGTCGGGGGGCAACCACAATTCACCCCGGCTGTATTCCACTCGGCCCCGGTGACCCTTTTGGTGTCGCCCCCGGACACCGTGCCCGTGCCCCGGTAC \
> +SRR1222430.37 37 length=251 \
> A3>333BFA2FF4GBFFDGGCGED?FGEGGHGFFFEEG?AF13@50>///11B13@@1/>//B0?>////<//B/00??@/--:--.;:C000;:0/0009.-9:.00:-.;9/9...-;.--9@--9:////-9-9..////9/;//;9///.9-..--------..99.9.//////;-;--9-.////://9/9.;.-;-99-.//.;////-;?9;...9-9-----9;-.;.;/.-9.;/99=--;"

mkdir tmp
echo "noNQ_Thres"
../src/rock -C 100 -k 30 -c 1 -l 2 -p -o ${srcdir}/data/iofiles.args/output_files_noNQ_Thres.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 150

echo "noNQ_Thres_very_low"
../src/rock -C 100 -k 30 -c 1 -l 2 -p -q 2 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_very_low.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 151

ret1=`diff tmp/klebsiella_100_1_very_low_qual_thres.fq tmp/klebsiella_100_1_no_qual_thres.fq|wc -l`
echo "before exit 142" 
test $ret1 -eq 0 || exit 152
ret2=`diff tmp/klebsiella_100_2_very_low_qual_thres.fq tmp/klebsiella_100_2_no_qual_thres.fq|wc -l`
echo "before exit 143" 
test $ret2 -eq 0 || exit 153

echo "q 12"
../src/rock -C 100 -k 30 -c 1 -l 2 -p -q 12 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_12.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null ||exit 154

echo "q 13"
../src/rock -C 100 -k 30 -c 1 -l 2 -p -q 13 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null ||exit 155

ret1=`diff tmp/klebsiella_100_1_13_qual_thres.fq tmp/klebsiella_100_1_12_qual_thres.fq|grep -c "@SRR1222430.14"`
echo "before exit 146" 
test $ret1 -eq  1 || exit 156

ret1=`diff tmp/klebsiella_100_1_13_qual_thres.fq tmp/klebsiella_100_1_12_qual_thres.fq|grep -c "@SRR1222430.24"`
echo "before exit 1461"
test $ret1 -eq  1 || exit 157

ret2=`diff tmp/klebsiella_100_1_13_qual_thres.fq tmp/klebsiella_100_1_12_qual_thres.fq|grep -c "length"`
echo "before exit 147"
test $ret2 -eq  4 || exit 158

ret1=`diff tmp/klebsiella_100_2_13_qual_thres.fq tmp/klebsiella_100_2_12_qual_thres.fq|grep -c "@SRR1222430.14"`
echo "before exit 148"
test $ret1 -eq  1 || exit 159

ret1=`diff tmp/klebsiella_100_2_13_qual_thres.fq tmp/klebsiella_100_2_12_qual_thres.fq|grep -c "@SRR1222430.24"`
echo "before exit 1481"
test $ret1 -eq  1 || exit 160

ret2=`diff tmp/klebsiella_100_2_13_qual_thres.fq tmp/klebsiella_100_2_12_qual_thres.fq|grep -c "length"`
echo "before exit 149"
test $ret2 -eq  4 || exit 161

rm -fr tmp

echo " "
echo "##################################################################################"
echo "testing rock with a quality score threshold for nucleotides and PE processed as single."

expected_diff1="304a305,308 \
  > @SRR1222430.37 37 length=250 \
> GCCTTTTCTTTTTCCAGGGAAAACCATCCAGGAGGAACTTTATTATGGCGATGTATGAAGTCGGTACCGTCACGGGTGCCGCGTCGCAGGCACGGGTGACAGGTGCGACAACAAAATGGTCACAGGAGGCGCTGGGGATACAGCCCGGGTCGATTCTGGTGGTCTACCGCAGCGGTAGTGCTGACCTGTATGCGATCAAATCCGTGGACAGCGACACGCAACTGACGCTGACCCGGAATATCACCACCGC \
> +SRR1222430.37 37 length=250 \
> AAAABFFFFFFFGCGFGGEFFCHHEEFHHGHHAFEAGAGGGFGGFHGFHGEEEFF5BDHHBDEGCEEHGAEGGFFGFCFHCGGC@E/EEEEHFEC@BBFFFG/GFEFDDCDHHGCC<?FGDDGHFFFGFFCGGG-.AGDGBGGHHGGG?ACGCFGG99B9;?C99B0CDGGCFF;DDFFFFFFFB99:;BBF/A.:9DFFF/F?EEFFFFFF>ADAFDFFFFFFE/ADADDFFFFFA;EEFF/BFFEEFF"

expected_diff2="> @SRR1222430.37 37 length=251 \
> GCTGGCCAGCTGGTTAGCAAACGACGAGGTGCTGGCGGGTTTAGCGGGAAAAATTGCGGAAGAGGCGCCGGGAAAAGCGGGGGTGTTATTACAGGTCTACGGCAGATGCGTGTCGCTGGCCGCGGCTTTTAGCGCCTACAAGTCTGGACTTCCCCGTCGGGGGGCAACCACAATTCACCCCGGCTGTATTCCACTCGGCCCCGGTGACCCTTTTGGTGTCGCCCCCGGACACCGTGCCCGTGCCCCGGTAC \
> +SRR1222430.37 37 length=251 \
> A3>333BFA2FF4GBFFDGGCGED?FGEGGHGFFFEEG?AF13@50>///11B13@@1/>//B0?>////<//B/00??@/--:--.;:C000;:0/0009.-9:.00:-.;9/9...-;.--9@--9:////-9-9..////9/;//;9///.9-..--------..99.9.//////;-;--9-.////://9/9.;.-;-99-.//.;////-;?9;...9-9-----9;-.;.;/.-9.;/99=--;"

mkdir tmp
../src/rock -C 100 -k 30 -c 1 -l 2  -o ${srcdir}/data/iofiles.args/output_files_noNQ_Thres.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 170

../src/rock -C 100 -k 30 -c 1 -l 2 -q 2 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_very_low.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 171

ret1=`diff tmp/klebsiella_100_1_very_low_qual_thres.fq tmp/klebsiella_100_1_no_qual_thres.fq|wc -l`
test $ret1 -eq 0 || exit 172
ret2=`diff tmp/klebsiella_100_2_very_low_qual_thres.fq tmp/klebsiella_100_2_no_qual_thres.fq|wc -l`
test $ret2 -eq 0 || exit 173

../src/rock -C 100 -k 30 -c 1 -l 2 -q 12 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_12.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null ||exit 174

../src/rock -C 100 -k 30 -c 1 -l 2 -q 13 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null ||exit 175

ret1=`diff tmp/klebsiella_100_1_13_qual_thres.fq tmp/klebsiella_100_1_12_qual_thres.fq|grep -c "@SRR1222430.37"`
test $ret1 -eq  1 || exit 176

ret2=`diff tmp/klebsiella_100_1_13_qual_thres.fq tmp/klebsiella_100_1_12_qual_thres.fq|grep -c "length"`
test $ret2 -eq  2 || exit 177

ret1=`diff tmp/klebsiella_100_2_13_qual_thres.fq tmp/klebsiella_100_2_12_qual_thres.fq|grep -c "@SRR1222430.37"`
test $ret1 -eq  1 || exit 178

ret2=`diff tmp/klebsiella_100_2_13_qual_thres.fq tmp/klebsiella_100_2_12_qual_thres.fq|grep -c "length"`
test $ret2 -eq  2 || exit 179

rm -fr tmp

echo " "
echo "##################################################################################"
echo "testing ROCK with a quality score threshold for nucleotides,minimum number of valid k-mer to keep a read and PE processed separately."

mkdir tmp
echo "../src/rock -C 100 -k 30 -c 1 -p -l 2 -o ${srcdir}/data/iofiles.args/output_files_noNQ_Thres.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -C 100 -k 30 -c 1 -l 2 -p -o ${srcdir}/data/iofiles.args/output_files_noNQ_Thres.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 180
echo "../src/rock -C 100 -k 30 -c 1 -p -l 2 -q 2 -m 5 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_very_low.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -C 100 -k 30 -c 1 -l 2 -p -q 2 -m 5 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_very_low.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 181

ret1=`diff tmp/klebsiella_100_1_very_low_qual_thres.fq tmp/klebsiella_100_1_no_qual_thres.fq|wc -l`
test $ret1 -eq 0 || exit 182
ret2=`diff tmp/klebsiella_100_2_very_low_qual_thres.fq tmp/klebsiella_100_2_no_qual_thres.fq|wc -l`
test $ret2 -eq 0 || exit 183


# All reads should be rejected.
echo "rock -k 30 -C 100 -c 1 -l 2 -q 13 -p -m 500 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -k 30 -C 100 -c 1 -l 2 -q 13 -p -m 500 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 184
[ -f "tmp/klebsiella_100_1_13_qual_thres.fq" ] || exit 185
[ -f "tmp/klebsiella_100_2_13_qual_thres.fq" ] || exit 186

echo "both files are here"

ret1=`cat tmp/klebsiella_100_1_13_qual_thres.fq|wc -l`
ret2=`cat tmp/klebsiella_100_2_13_qual_thres.fq|wc -l`

echo "ret1="$ret1
echo "ret2="$ret2

test $ret1 -eq 0 || exit 187
test $ret2 -eq 0 || exit 188



echo "rock -k 30 -C 100 -c 1 -l 2 -q 13 -p -m 300 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -k 30 -C 100 -c 1 -l 2 -q 13 -p -m 300 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 189

[ -f "tmp/klebsiella_100_1_13_qual_thres.fq" ] || exit 190
[ -f "tmp/klebsiella_100_2_13_qual_thres.fq" ] || exit 191

ret1=`cat tmp/klebsiella_100_1_13_qual_thres.fq|wc -l`
ret2=`cat tmp/klebsiella_100_2_13_qual_thres.fq|wc -l`

test $ret1 -eq 264 || exit 192
test $ret2 -eq 264 || exit 193

# check that reads that are kept are always the same.
lst1=`cat tmp/klebsiella_100_1_13_qual_thres.fq|grep @S|cut -d ' ' -f 1|sort`
lst2=`cat tmp/klebsiella_100_2_13_qual_thres.fq|grep @S|cut -d ' ' -f 1|sort`

lst_ref=`cat ${srcdir}/data/non_regression/ids_read_kept_q13m300.txt`

test "$lst1" = "$lst2" || exit 194
test "$lst_ref" = "$lst1" || exit 195

rm -fr tmp

echo "##################################################################################"
echo "testing ROCK with a quality score threshold for nucleotides,minimum number of valid k-mer to keep a read and PE processed as single."

mkdir tmp
echo "../src/rock -C 100 -k 30 -c 1 -l 2 -o ${srcdir}/data/iofiles.args/output_files_noNQ_Thres.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -C 100 -k 30 -c 1 -l 2 -o ${srcdir}/data/iofiles.args/output_files_noNQ_Thres.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 200
echo "../src/rock -C 100 -k 30 -c 1 -l 2 -q 2 -m 5 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_very_low.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -C 100 -k 30 -c 1 -l 2 -q 2 -m 5 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_very_low.txt  ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 201

ret1=`diff tmp/klebsiella_100_1_very_low_qual_thres.fq tmp/klebsiella_100_1_no_qual_thres.fq|wc -l`
test $ret1 -eq 0 || exit 202
ret2=`diff tmp/klebsiella_100_2_very_low_qual_thres.fq tmp/klebsiella_100_2_no_qual_thres.fq|wc -l`
test $ret2 -eq 0 || exit 203

# All reads should be rejected.
echo "rock -k 30 -C 100 -c 1 -l 2 -q 13 -m 500 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -k 30 -C 100 -c 1 -l 2 -q 13 -m 500 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 204
[ -f "tmp/klebsiella_100_1_13_qual_thres.fq" ] || exit 205
[ -f "tmp/klebsiella_100_2_13_qual_thres.fq" ] || exit 206

echo "both files are here"

ret1=`cat tmp/klebsiella_100_1_13_qual_thres.fq|wc -l`
ret2=`cat tmp/klebsiella_100_2_13_qual_thres.fq|wc -l`

echo "ret1="$ret1
echo "ret2="$ret2

test $ret1 -eq 0 || exit 161
test $ret2 -eq 0 || exit 162


echo "rock -k 30 -C 100 -c 1 -l 2 -q 13 -m 300 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq"
../src/rock -k 30 -C 100 -c 1 -l 2 -q 13 -m 300 -o ${srcdir}/data/iofiles.args/output_files_NQ_Thres_13.txt ${srcdir}/data/fastq.raw/klebsiella_100_1.fq,${srcdir}/data/fastq.raw/klebsiella_100_2.fq >/dev/null || exit 207

[ -f "tmp/klebsiella_100_1_13_qual_thres.fq" ] || exit 208
[ -f "tmp/klebsiella_100_2_13_qual_thres.fq" ] || exit 209

ret1=`cat tmp/klebsiella_100_1_13_qual_thres.fq|wc -l`
ret2=`cat tmp/klebsiella_100_2_13_qual_thres.fq|wc -l`

test $ret1 -eq 264 || exit 210
test $ret2 -eq 264 || exit 211

# check that reads that are kept are always the same.
lst1=`cat tmp/klebsiella_100_1_13_qual_thres.fq|grep @S|cut -d ' ' -f 1|sort`
lst2=`cat tmp/klebsiella_100_2_13_qual_thres.fq|grep @S|cut -d ' ' -f 1|sort`

lst_ref=`cat ${srcdir}/data/non_regression/ids_read_kept_q13m300.txt`

test "$lst1" = "$lst2" || exit 212
test "$lst_ref" = "$lst1" || exit 213

rm -fr tmp


echo " "
echo "##################################################################################"
echo "unit testing for cms component"
../src/unit_test_cms || exit 240

exit 0

     
     
