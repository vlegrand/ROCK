#!/bin/sh

## Debug mode
test "x$VERBOSE" = "xx" && set -x

[ -z "$srcdir" ] && srcdir="."
echo "srcdir"=${srcdir}

## doing some cleanup in case previous execution failed. 
rm -fr data/fastq.filtered || echo "nothing to clean up"

## check options
echo " "
echo "##################################################################################"
echo "checking rock options and error mesages"
#../src/rock|grep "input file name is mandatory"  >/dev/null || exit 1
../src/rock|grep -i "usage"  >/dev/null || exit 2
../src/rock -l 1 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt fic1.fq fic2.fq|grep "It cannot be both"  >/dev/null ||exit 3
#../src/rock -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "usage"  >/dev/null ||exit 4
#../src/rock -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt
../src/rock -l 1 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "parent directory for output files:" >/dev/null ||exit 5 
../src/rock -l 1 -f 0 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "maximum">/dev/null||exit 51
../src/rock -l 1 -f 1.1 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "maximum">/dev/null||exit 52

echo " "
echo "##################################################################################"
echo "creating directory for test output files"
[ -d "data" ] || mkdir data
echo "copying input data for tests" 
if [ ! -d "data/fastq.raw" ] 
then
    mkdir data/fastq.raw 
    cp -R ${srcdir}/data/fastq.raw data/ 
    erase_indata=true
fi  
if [ ! -d "data/unit" ] 
then
    mkdir data/unit 
    cp -R ${srcdir}/data/unit data/
    chmod u+w data/unit
fi
if [ ! -d "data/iofiles.args" ]
then
    mkdir  data/iofiles.args
    cp -R ${srcdir}/data/iofiles.args data/
fi
echo "checking content of local data directory"
ls -l data
echo "checking content of local data/fastq.raw directory"
ls -l data/fastq.raw/
echo "checking content of local data/iofiles.args directory"
ls -l data/iofiles.args

mkdir data/fastq.filtered || exit 6


echo " "
echo "##################################################################################"
echo "doing more checks on options and error messages"
../src/rock -l 1 -C 500000  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "Bad value for kappa"  >/dev/null || exit 7
../src/rock -l 1 -C 500 -c 600  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "ERROR lower filter is higher than high filter" >/dev/null || exit 8
../src/rock -l 1 -C 500 -c 400 -k 60  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "Bad value for k" >/dev/null || exit 9
../src/rock -C 500 -c 400 -k 10 -l 0  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "Bad value for lambda" >/dev/null || exit 10
../src/rock -C 500 -c 400 -k 10 -l 500  -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "Not enough RAM on the machine" >/dev/null || exit 11
#../src/rock -C 500 -c 400 -k 10 -l 12 -g 25 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "options are mutually exclusive" >/dev/null|| exit 12
../src/rock -C 500 -c 400 -k 10 -n 50000000000 -c 1 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "think of increasing the value for the low filter" >/dev/null || exit 13
../src/rock -C 500 -c 400 -k 10 -l 12 -n 85000000 -i ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100.txt -o ${srcdir}/data/iofiles.args/extract_klebsiella_long_reads_100_filtered.txt|grep "options are mutually exclusive">/dev/null || exit 14
../src/rock -l 1 -C 500 -c 400 -k 10 -q 3 -m 0  -i ${srcdir}/test/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "minimum number of valid k-mer for keeping a read must be an integer">/dev/null || exit 15
../src/rock -l 1 -C 500 -c 400 -k 10 -q -1 -m 2  -i ${srcdir}/test/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "q must be">/dev/null || exit 16
#../src/rock -C 500 -c 400 -k 10 -q 4 -m 2 -p 10 -i ${srcdir}/test/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "p option is a flag. You cannot provide a value for it">/dev/null || exit 17
#../src/rock -C 500 -c 400 -k 10 -q 4 -m 2 -p 1,5 -i ${srcdir}/test/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "value for -p option must be 0, 1 or 2">/dev/null || exit 18
#../src/rock -C 500 -c 400 -k 10 -q 4 -m 2 -p -1 -i ${srcdir}/test/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "value for -p option must be 0, 1 or 2">/dev/null || exit 17

#../src/rock -C 500 -c 400 -k 10 -q 2 -m 2 -i ${srcdir}/test/data/iofiles.args/extract_klebsiella_long_reads_100.txt|grep "Incompatible options">/dev/null || exit 161

# here check that we have enough memory for running the tests.
../src/unit_test_cms
if [ $? = 0 ] ## we have enough memory to run the tests in rock_mem.sh
then 
     ${srcdir}/rock_mem.sh ||exit 255
fi

# unit tests
echo " "
echo "##################################################################################"
echo "running unit tests"
echo "unit testing for fqreader"
../src/unit_test_fqreader || exit 22

echo " "
echo "##################################################################################"
echo "unit testing for read utils"
../src/unit_test_read_utils || exit 23


echo " "
echo "##################################################################################"
echo "unit testing for fqwriter"
../src/unit_test_fqwriter || exit 24

echo " "
echo "#################################################################################"
echo "unit testing for math utils"
../src/unit_test_math_utils || exit 25

# cleanup
echo " "
echo "##################################################################################"
echo "cleaning"
rm -fr data/fastq.filtered || exit 21
echo "erase_indata="$erase_indata
if [ "$erase_indata" = "true" ] 
then
    echo "erasing data/fastq.raw"
    rm -fr data/fastq.raw || exit 26
fi
if [ "$erase_indata" = "true" ] 
then
   echo "erasing data/unit"
   rm -fr data/unit || exit 27
fi
if [ "$erase_indata" = "true" ] 
then
   echo "erasing data/iofiles.args"
   rm -fr data/iofiles.args || exit 28
fi


## Normal end
exit 0
