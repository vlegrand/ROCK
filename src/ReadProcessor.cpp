/*
  
  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#include <stdlib.h>
#include "ReadProcessor.h"
#include <iostream>

//! Tried to inline this but slightly slower. Stays here.
unsigned long ReadProcessor::nucleoToNumberWrap(char s) {
    (s=='N')?n=k:n;
    n--;
    return nucleoToNumber(s);
}

void ReadProcessor::kMerAndRevToNumber(char * k_m,KmRevNumbers& numbers) {
    unsigned long c,crev;
    if (numbers.first) { // first k_mer conversion
        int i;
        int j=k;
        for (i=0;i<k;i++) {
            c=nucleoToNumberWrap(k_m[i]); // do not catch exception for the moment. If nucleoToNumber throws -1 and there is no catch clause for it, the terminate() function will be called automatically. Anyway, there's not munch we can do if it returns -1.
            numbers.nbr=numbers.nbr<<2;
            numbers.nbr=numbers.nbr|c;
            crev=nucleoToNumberReverse(k_m[j-1],j);
            numbers.nbr_rev=numbers.nbr_rev|crev;
            j--;
        }
        numbers.first=0;
    } else {
        c=nucleoToNumberWrap(k_m[k-1]);
        numbers.nbr=numbers.nbr<<2;
        numbers.nbr=numbers.nbr&mask_kMer;
        numbers.nbr=numbers.nbr|c;
        crev=nucleoToNumberReverse(k_m[k-1],k);
        numbers.nbr_rev=numbers.nbr_rev>>2;
        numbers.nbr_rev=numbers.nbr_rev|crev;
    }
}



void ReadProcessor::getKMerNumbers(char * dnaStr,int l,readNumericValues& all_values) { // See simple_test.cpp and results. benchmark showed that std::vector is very slightly faster than C array and doesn't require more memory in our case. So, I am using it since it makes code simpler.
    int i;
    KmRevNumbers numbers;
    initNumbers(numbers);
    char * p_char=dnaStr;
    int nb_k_m=l-k+1;
    for (i=0; i<nb_k_m;i++) {
    	kMerAndRevToNumber(p_char,numbers);
    	if (n<0) {
    		(numbers.nbr<numbers.nbr_rev)?all_values.push_back(numbers.nbr_rev):all_values.push_back(numbers.nbr); // finding a k-mer is equivalent to finding its reverse complement; as numeric values for k-mers are unique, just take into account the biggest of the 2.
    	}
        p_char++;
    }
}
