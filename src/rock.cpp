/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

//#include <sys/sysctl.h>
#include <cstdlib>
#include "err.h"
#include <string.h>

#include <iostream>
#include <iomanip>
#include <map>
#include <vector>
#include <unistd.h>

#include "math_utils.h"
#include "CountMinSketch.hpp"
#include "rock_commons.h"
#include "srp.h"

#include "FqMainBackend.h"
#include "fqreader.h"
#include "fqwriter.h"
#include "Filter.hpp"
#include "ROCKparams.h"


using namespace std;


// #define BENCHMARK
#ifdef BENCHMARK
#include <sys/time.h>
#include <sys/resource.h>
void printRUsage() {
    struct rusage usage;
    int res=getrusage(RUSAGE_SELF,&usage);
    std::cout<<"memory info: maxrss="<<usage.ru_maxrss<<" ixrss="<<usage.ru_ixrss<<" idrss="<<usage.ru_idrss<<" isrss="<<usage.ru_isrss<<" minflt="<<usage.ru_minflt<<" majflt="<<usage.ru_majflt<<endl;
    std::cout<<"time info: user time="<<usage.ru_utime.tv_sec<<" system time="<<usage.ru_stime.tv_sec<<endl;
}
#endif

// method used for debug only
void printFastqQualThreshold(const FasqQualThreshold& q) {
    cout<<"nucl_score_threshold="<<q.nucl_score_threshold<<endl;
    cout<<"min_correct_k_mers_in_read="<<q.min_correct_k_mers_in_read<<endl;
    cout<<"k="<<q.k<<endl;
}


// function used for debugging purpose only
void printCMSparams(const CMSparams& p) {
    cout<<"CMSparams content:"<<endl;
    cout<<"lambda="<<p.lambda<<endl;
    cout<<"kappa="<<p.kappa<<endl;
    cout<<"kappa_prime="<<p.kappa_prime<<endl;
    cout<<"filter_size="<<p.max_filter_size<<endl;
}

const void printVerboseInfo(const ROCKparams& Rparms,const Filter& filter,const T_read_counters& rc,\
					  const unsigned long& tot_nb_kmers) {
    cout<<setw(40)<<left<<"total available RAM (Gb)"<<getNodePhysMemory()<<endl;
	std::vector<IO_fq_files>::iterator it_s;
	vector<PE_files>::iterator it_pe;
	vector<PE_files> v_PE_files=Rparms.get_PE_files();
	vector<IO_fq_files> single_files=Rparms.get_single_files();
	for (it_pe=v_PE_files.begin();it_pe!=v_PE_files.end();it_pe++) {
		cout<<setw(40)<<left<<"PE input files"<<it_pe->PE1.in_fq_file<<" "<<it_pe->PE2.in_fq_file<<endl;
	}
	for (it_s=single_files.begin();it_s!=single_files.end();it_s++) {
		cout<<setw(40)<<left<<"SE input file(s)"<<it_s->in_fq_file<<endl;
	}
	CMSparams parms=Rparms.getCMSparams();
	cout<<setw(40)<<left<<"upper-bound coverage depth"<<parms.kappa<<endl;
	cout<<setw(40)<<left<<"lower-bound coverage depth"<<parms.kappa_prime<<endl;
	cout<<setw(40)<<left<<"k-mer length"<<Rparms.get_k()<<endl;
	cout<<setw(40)<<left<<"expected no. distinct k-mers"<<Rparms.get_expected_nbKmers()<<endl;
	cout<<setw(40)<<left<<"no. hash. function(s)"<<parms.lambda<<endl;

	cout<<setw(40)<<left<<"expected false positive proba."<<setprecision (6)<<fixed<<Rparms.get_expected_collision_proba()<<" (cov. depth > 1)"<<endl;

    for (int i=0;i<parms.lambda;i++) {
        string tmp="no. buckets for hash. ";
        tmp+=to_string(i+1);
        cout<<setw(40)<<left<<tmp<<Pi_js[i]<<endl;
    }
	cout<<setw(40)<<left<<"no. bits per bucket"<<filter.getNbBitsForCounters()<<endl;
	cout<<setw(40)<<left<<"count-min sketch size (Gb)"<<Rparms.getFilterSize()<<endl;
	cout<<setw(40)<<left<<"no. input reads"<<rc.nb_input_reads<<endl;
	cout<<setw(40)<<left<<"no. input k-mers"<<tot_nb_kmers<<endl;
	unsigned long approx_nbDiffKm=filter.getApproxNbDistinctKMers();
	std::vector<unsigned long> zeroes=filter.getUnsetBuckets();
	for (int i=0;i<=parms.lambda-1;i++){
	    string tmp="no. unset buckets for hash. ";
	    tmp+=to_string(i+1);
	    cout<<setw(40)<<left<<tmp<<zeroes[parms.lambda-1-i]<<endl;
	}
	cout<<setw(40)<<left<<"approx. no. distinct k-mers"<<approx_nbDiffKm<<endl;
	int smallest_kappa=parms.kappa;
	if (parms.kappa_prime>0) smallest_kappa=parms.kappa_prime;
	float p =getCollisionProba(smallest_kappa,approx_nbDiffKm,Pi_js[0],parms.lambda);
	cout<<setw(40)<<left<<"approx. false positive proba."<<setprecision (6)<<p<<endl;
	cout<<setw(40)<<left<<"no. selected reads"<<rc.nb_output_reads<<endl;
	for (it_pe=v_PE_files.begin();it_pe!=v_PE_files.end();it_pe++) {
		// cout<<setw(40)<<left<<"PE output files"<<it_pe->PE1.out_fq_file<<" "<<it_pe->PE2.out_fq_file<<" "<<it_pe->PE1.undef_fq_file<<" "<<it_pe->PE2.undef_fq_file<<endl;
		cout<<setw(40)<<left<<"PE output files"<<it_pe->PE1.out_fq_file<<" "<<it_pe->PE2.out_fq_file<<endl;
	}
    for (it_s=single_files.begin();it_s!=single_files.end();it_s++) {
		cout<<setw(40)<<left<<"SE output files"<<it_s->out_fq_file<<endl;
    }
}



int main(int argc,char * argv[]) {
#ifdef BENCHMARK
    cout<<"program startup"<<endl;
    printRUsage();
#endif
    srp sr;
    T_read_counters rc;
    ROCKparams main_parms;
    main_parms.initFromMainOptsArgs(argc,argv);
    int f_id=main_parms.get_f_id();
    CMSparams parms=main_parms.getCMSparams();
    FasqQualThreshold qual_thres=main_parms.getQualThresholds();
    std::vector<IO_fq_files> single_files=main_parms.get_single_files();
    vector<PE_files> v_PE_files=main_parms.get_PE_files();

    FqBaseBackend * map_id_backend[k_max_input_files];
    Filter the_filter(parms,qual_thres);

#ifdef BENCHMARK
    cout<<"processed input args; going to start reading fastq files"<<endl;
    printRUsage();
#endif
    unsigned long tot_nb_kmers=processInputFiles(single_files,v_PE_files,map_id_backend,main_parms.getQualThreshold(),&sr,0); // Removing -p option, set default mode to 0 (PE processed as single).
#ifdef BENCHMARK
    cout<<"finished loading fastq file into sr structure"<<endl;
    cout<<"size of srp structure="<<sizeof(sr)<<endl;
    printRUsage();
    cout<<"Now going to fill CMS"<<endl;
#endif

    int k=main_parms.get_k();

    the_filter.fillCMS(map_id_backend,f_id,k, &sr);

#ifdef BENCHMARK
    cout<<"finished filling CCountMinSketch"<<endl;
    // cout<<"size of CountMinSketch object="<<sizeof(*pcms)<<endl;
    printRUsage();
    cout<<"going to get approximated number of distinct k-mers in the CMS."<<endl;
#endif

    unsigned long approx_nb_k_mers=the_filter.getApproxNbDistinctKMers();
#ifdef BENCHMARK
    printRUsage();
    cout<<"found "<<approx_nb_k_mers<<" distinct k-mers"<<endl;
    cout<<"Now going to remove read with cov<kappa_prime"<<endl;
#endif

    // main_parms.setFilterSize(the_filter.getSize()); // no more used max_rss is not precise enough
    // Now, remove reads that are beneath kappa_prime
    if (parms.kappa_prime) the_filter.lowFilterCMS(map_id_backend,f_id,k,&sr);

#ifdef BENCHMARK
    cout<<"finished remove read with cov<kappa_prime"<<endl;
    // cout<<"size of CountMinSketch object="<<sizeof(*pcms)<<endl;
    printRUsage();
    cout<<"Should now write output files"<<endl;
#endif
    writeFilteredFastq(map_id_backend,f_id,rc,sr);
#ifdef BENCHMARK
    cout<<"finished writing filtered reads"<<endl;
    printRUsage();
    cout<<"Should now free memory"<<endl;
#endif
    int i;
    for (i=1;i<=f_id;i++) {
       // cout<<"deleting backend: "<<i-1<<endl;
        delete map_id_backend[i-1];
    }

#ifdef BENCHMARK
    cout<<"going to compute collision probability"<<endl;
    printRUsage();
#endif
    /*
    int smallest_kappa=parms.kappa;
    if (parms.kappa_prime>0) smallest_kappa=parms.kappa_prime;
    float p =getCollisionProba(smallest_kappa,approx_nb_k_mers,Pi_js[0],parms.lambda);
    cout<<"estimated probability of collision:"<<p<<endl;
    cout<<"estimated number of distinct k-mers:"<<approx_nb_k_mers<<endl;
    */
    if (main_parms.is_verbose()) printVerboseInfo(main_parms,the_filter,rc,tot_nb_kmers);

#ifdef BENCHMARK
    cout<<"finished,going to exit "<<endl;
    printRUsage();
#endif
	return EXIT_SUCCESS;
}
