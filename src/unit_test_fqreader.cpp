/*
  Unit and non regression testing for the fqreader component.
  I keep using assert for the moment, don't want to add a dependency on boost (or any other test framework) just for the tests.

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include "rock_commons.h"
#include "FqConstants.h"
#include "srp.h"
#include "FqMainBackend.h"
#include "fqreader.h"
#include "unit_tests_tools.h"


using namespace std;

const int treat_PE_as_single=0;

void test_processSingleFile() {
    srp sr;
    unsigned char f_id=1;
    //FqMainBackend::setTreatPEMode(0);
    processSingleFile((char *) "../test/data/unit/test_single.fq",f_id,&sr);
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;

    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        //cout << "score="<<rit->first<<endl;
        unsigned long score=rit->first;
        assert(score==10); // reads are all 152 character long in this test.
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            cout<<"offset_quotient="<<offset_quotient<<endl;
	    assert(offset_quotient==0);
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                unsigned char fid_stored=it_struct->fileid;
                assert(fid_stored >>4==f_id);
                if (cnt_read==0) assert(it_struct->read_a1==0);
                if (cnt_read==1) assert(it_struct->read_a1==350);
                if (cnt_read==2) assert(it_struct->read_a1==699);
                if (cnt_read==3) assert(it_struct->read_a1==1049);
                if (cnt_read==4) assert(it_struct->read_a1==1400);
                if (cnt_read==5) assert(it_struct->read_a1==1751);
                cnt_read++;
                /*int tmp=fid_stored >>4;
                cout<<" fileid="<<tmp<<" read_a1="<<it_struct->read_a1<<endl;*/
            }
        }
    }
    assert(cnt_read==6);
}

/*
 * Expects a given minimum of k_mers to be present in the read.
 */
void test_processSingleFileWithMQOption() {
    srp sr;
    unsigned char f_id=1;
    FasqQualThreshold qual_thres;
    qual_thres.k=10;
    qual_thres.nucl_score_threshold=14+k_phred_32;
    qual_thres.min_correct_k_mers_in_read=100;
    FqBaseBackend::setQualThreshold(qual_thres);
    //FqMainBackend::setTreatPEMode(0);
    FqMainBackend be_fq=FqMainBackend(&sr);
    
    //be_fq.setUndefFile((char *) "../test/data/unit/test_single.undef.fq");
    be_fq.processFile((char *) "../test/data/unit/test_single.fq",f_id);

    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;

    int cnt_read=0;
    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==6);
    //assert(remove((char *) "../test/data/unit/test_single.undef.fq")==0);
    sr.clear();

    cnt_read=0;
    qual_thres.min_correct_k_mers_in_read=130;
    FqBaseBackend::setQualThreshold(qual_thres);
    FqMainBackend be_fq2=FqMainBackend(&sr);
    //be_fq2.setUndefFile((char *) "../test/data/unit/test_single2.undef.fq");
    be_fq2.processFile((char *) "../test/data/unit/test_single.fq",f_id);

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
              if (cnt_read==1) assert(it_struct->read_a1==0);
              if (cnt_read==2) assert(it_struct->read_a1==1400);
              if (cnt_read==3) assert(it_struct->read_a1==1751);
            }
        }
    }
    assert(cnt_read==3);
    //assert(remove((char *) "../test/data/unit/test_single2.undef.fq")==0);
    sr.clear();

    cnt_read=0;
    qual_thres.nucl_score_threshold=36+k_phred_32;
    qual_thres.k=30;
    FqBaseBackend::setQualThreshold(qual_thres);

    FqMainBackend be_fq3=FqMainBackend(&sr);
    //be_fq3.setUndefFile((char *) "../test/data/unit/test_single3.undef.fq");
    be_fq3.processFile((char *) "../test/data/unit/test_single.fq",f_id);

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==0);
    //assert(remove((char *) "../test/data/unit/test_single3.undef.fq")==0);
}

/*
 * Test case with other data than those I had until here.
 * Quality score contains '@'caracter (usually start of fastq record),
 * reads are longer,
 * id and '+' line contain additional information.
 */
void test_processPEfilesWithA() {

    char * fq_3_test=(char *) "../test/data/unit/klebsiella_PE1.fq";
    char * fq_4_test=(char *) "../test/data/unit/klebsiella_PE2.fq";

    unsigned char f_id3=3;
    unsigned char f_id4=4;

    srp sr;
    //FqMainBackend::setTreatPEMode(0);
    processPEFiles(fq_3_test, f_id3,fq_4_test, f_id4,&sr);
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    unsigned char masque=0x0F;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        cout << "score="<<rit->first<<endl;
        unsigned long score=rit->first;
        /*if (cnt_read==0 || cnt_read==1) assert(score==10);
        if (cnt_read==2) assert(score==9);*/
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            assert(offset_quotient==0);
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                unsigned char fid_stored=it_struct->fileid;
                assert(fid_stored >>4==f_id3);
                assert((fid_stored &masque)==f_id4);
                if (cnt_read==0) {
                    assert(it_struct->read_a1==0);
                    assert(it_struct->read_a2==0);
                }
                if (cnt_read==4) {
                    // std::cout<<it_struct->read_a1<<" "<<it_struct->read_a2;
                    assert(score==33);
                    assert(it_struct->read_a1==558);
                    assert(it_struct->read_a2==-2);
                }
                if (cnt_read==5) {
                    // std::cout<<it_struct->read_a1<<" "<<it_struct->read_a2;
                    assert(score==33);
                    assert(it_struct->read_a1==1114);
                    assert(it_struct->read_a2==0);
                }
                cnt_read++;

                int tmp1=fid_stored >>4;
                int tmp2=fid_stored &masque;
                cout<<" fileid1="<<tmp1<<" read_a1="<<it_struct->read_a1<<endl;
                cout<<" fileid2="<<tmp2<<" read_a2="<<it_struct->read_a2<<endl;
            }
        }
    }
    assert(cnt_read==10);
}

void test_processPEFiles() {
    char * fq_1_test=(char *) "../test/data/unit/test_PE1.fq";
    char * fq_2_test=(char *) "../test/data/unit/test_PE2.fq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;

    srp sr;
    //FqMainBackend::setTreatPEMode(0);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr);
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    unsigned char masque=0x0F;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        // cout << "score="<<rit->first<<endl;
        unsigned long score=rit->first;
        if (cnt_read==0 || cnt_read==1) assert(score==20);
        if (cnt_read==2) assert(score==19);
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            assert(offset_quotient==0);
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                unsigned char fid_stored=it_struct->fileid;
                assert(fid_stored >>4==f_id1);
                assert((fid_stored &masque)==f_id2);
                if (cnt_read==0) {
                    assert(it_struct->read_a1==0);
                    assert(it_struct->read_a1==0);
                }
                if (cnt_read==1) {
                    std::cout<<it_struct->read_a1<<" "<<it_struct->read_a2;
                    assert(it_struct->read_a1==349);
                    assert(it_struct->read_a2==0);
                }
                if (cnt_read==2) {
                    std::cout<<it_struct->read_a1<<" "<<it_struct->read_a2;
                    assert(it_struct->read_a1==698);
                    assert(it_struct->read_a2==0);
                }
                cnt_read++;
                
                int tmp1=fid_stored >>4;
                int tmp2=fid_stored &masque;
                cout<<" fileid1="<<tmp1<<" read_a1="<<it_struct->read_a1<<endl;
                cout<<" fileid2="<<tmp2<<" read_a2="<<it_struct->read_a2<<endl;
            }
        }
    }
    assert(cnt_read==3);
}

/*
 * Auxilliary function for testing the processing of PE files while using m and q options.
 */
void aux_testPEFilesMQ(FasqQualThreshold qual_thres,int nb_expected_reads) {
    srp sr;
    char * fq_1_test=(char *) "../test/data/unit/09-4607_S43_R1.fastq";
    char * fq_2_test=(char *) "../test/data/unit/09-4607_S43_R2.fastq";
    //char * fq_1_test_undef=(char *) "../test/data/unit/09-4607_S43_R1.undef.fastq";
    //char * fq_2_test_undef=(char *) "../test/data/unit/09-4607_S43_R2.undef.fastq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;
    //FqMainBackend::setTreatPEMode(0);
    FqBaseBackend::setQualThreshold(qual_thres);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr);

    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        //unsigned long score=rit->first;
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
   /* assert(cnt_read==nb_expected_reads);
    assert(remove((char *) "../test/data/unit/09-4607_S43_R1.undef.fastq")==0);
    assert(remove((char *) "../test/data/unit/09-4607_S43_R2.undef.fastq")==0);*/
}

void test_processPEFilesWithMQOptions() {


    FasqQualThreshold qual_thres;
    qual_thres.k=20;
    qual_thres.nucl_score_threshold=14+k_phred_32;
    qual_thres.min_correct_k_mers_in_read=76;
    //FqMainBackend::setTreatPEMode(0);

    aux_testPEFilesMQ(qual_thres,4); // last fq records contains only 77 correct k-mers.

    qual_thres.min_correct_k_mers_in_read=100;
    aux_testPEFilesMQ(qual_thres,3);

    qual_thres.min_correct_k_mers_in_read=150;
    aux_testPEFilesMQ(qual_thres,2);

    qual_thres.min_correct_k_mers_in_read=180;
    aux_testPEFilesMQ(qual_thres,2);

    qual_thres.min_correct_k_mers_in_read=230;
    aux_testPEFilesMQ(qual_thres,0);
}


void check_processAllFilesResults(srp& sr) {
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;
    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        //cout << "score="<<rit->first<<endl;
        unsigned long score=rit->first;
        if (cnt_read==0 || cnt_read==1) assert(score==20);
        else if (cnt_read==2) assert(score==19);
        else {
            //cout << "score="<<rit->first<<endl;
            assert(score==10);
        }
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            assert(offset_quotient==0);
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==9);
}

void test_processAllFiles() {
    char * fq_1_test=(char *) "../test/data/unit/test_PE1.fq";
    char * fq_2_test=(char *) "../test/data/unit/test_PE2.fq";
    char * fq_single=(char *) "../test/data/unit/test_single.fq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;
    unsigned char f_single=3;

    srp sr;
    //FqMainBackend::setTreatPEMode(0);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr);
    processSingleFile(fq_single,f_single,&sr);

    check_processAllFilesResults(sr);
}

void test_processPE_nuclthreshold() {
    char * fq_1_test=(char *) "../test/data/unit/test_PE1_PE_not_as_single.fq";
    char * fq_2_test=(char *) "../test/data/unit/test_PE2_PE_not_as_single.fq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;
    srp sr;

    FasqQualThreshold qual_thres;
    qual_thres.k=10;
    qual_thres.min_correct_k_mers_in_read=1;
    qual_thres.nucl_score_threshold=2+k_phred_32;

    FqBaseBackend::setQualThreshold(qual_thres);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr,1000);

    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        //unsigned long score=rit->first;
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==3);


}


void test_processPE_nuclthreshold2() {
    char * fq_1_test=(char *) "../test/data/unit/test_PE1_PE_not_as_single_pathological.fq";
    char * fq_2_test=(char *) "../test/data/unit/test_PE2_PE_not_as_single_pathological.fq";

    char * fq_1_test_undef=(char *) "../test/data/unit/test_PE1_PE_not_as_single_pathological.undef.fastq";
    char * fq_2_test_undef=(char *) "../test/data/unit/test_PE2_PE_not_as_single_pathological.undef.fastq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;
    srp sr;

    FasqQualThreshold qual_thres;
    qual_thres.k=30;
    qual_thres.min_correct_k_mers_in_read=221;
    qual_thres.nucl_score_threshold=10+k_phred_32;

    FqBaseBackend::setQualThreshold(qual_thres);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr,1000);

    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==2);


}

void test_processPE_not_as_singleWithMQ() {
    char * fq_1_test=(char *) "../test/data/unit/fake_PE1.fq";
    char * fq_2_test=(char *) "../test/data/unit/fake_PE2.fq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;
    srp sr;

    FasqQualThreshold qual_thres;
    qual_thres.k=30;
    qual_thres.min_correct_k_mers_in_read=64;
    qual_thres.nucl_score_threshold=10+k_phred_32;

    FqBaseBackend::setQualThreshold(qual_thres);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr,1000);

    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==1);
    sr.clear();

    cout<<"try with a different min_correct_k_mers_in_read=87"<<endl;
    qual_thres.k=30;
    qual_thres.min_correct_k_mers_in_read=87;
    qual_thres.nucl_score_threshold=15+k_phred_32;

    FqBaseBackend::setQualThreshold(qual_thres);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr,1000);

    cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==0);
    sr.clear();

    cout<<"try with a different min_correct_k_mers_in_read=13"<<endl;

    qual_thres.k=30;
    qual_thres.min_correct_k_mers_in_read=13;
    qual_thres.nucl_score_threshold=18+k_phred_32;

    FqBaseBackend::setQualThreshold(qual_thres);
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr,1000);


    cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==1);
    sr.clear();

}


void test_processInputFiles() {
    char * fq_1_test=(char *) "../test/data/unit/test_PE1.fq";
    char * fq_2_test=(char *) "../test/data/unit/test_PE2.fq";
    char * fq_single=(char *) "../test/data/unit/test_single.fq";

    IO_fq_files s;
    s.in_fq_file=fq_single;

    vector<IO_fq_files> v_single;
    v_single.push_back(s);

    PE_files pe;
    pe.PE1.in_fq_file=fq_1_test;
    pe.PE2.in_fq_file=fq_2_test;
    vector<PE_files> v_pe;
    v_pe.push_back(pe);

    srp sr;
    FasqQualThreshold default_qual_thres;
    default_qual_thres.k=30;
    default_qual_thres.min_correct_k_mers_in_read=0; // aim of that test is not to check undef file creation. Disable it by putting 0 here
    default_qual_thres.nucl_score_threshold=0; // leave default values for that test
    
    //FqMainBackend::setTreatPEMode(treat_PE_as_single);

    FqBaseBackend * array_be[k_max_input_files];
    processInputFiles(v_single,v_pe,array_be,default_qual_thres,&sr,1);

    // check that result is correct in sr.
    check_processAllFilesResults(sr);

    // check that the 3 backends are correct
    FqMainBackend * pbe=(FqMainBackend *) array_be[0]; // TODO see if one can use check_case, static_cast or one of them if they are not in boost.
    FqAuxBackend * pbe2=(FqAuxBackend *) array_be[2];
    FqMainBackend * pbe3=(FqMainBackend *) array_be[1];
    
    assert(strcmp(pbe->i_filename,fq_single)==0);
    assert(pbe->f_id==1);
    assert(pbe->p_auxFqProcessor==NULL);

    assert(pbe3->p_auxFqProcessor==pbe2);
    assert(strcmp(pbe3->i_filename,fq_1_test)==0);
    assert(pbe3->f_id==2);

    assert(strcmp(pbe2->i_filename,fq_2_test)==0);
    assert(pbe2->f_id==3);


    int i;
    for (i=0;i<3;i++) delete array_be[i];
}

void test_processBufSingle() {
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;
    unsigned char f_id1=1;
    std::string bufstr="@NS500443:65:H573HAFXX:1:11101:20964:1048/1\n\
ACATTTCACTGGTCATGAGCTTGGTGAGAGAAGCGATACGAATGACCGAATCAAGCTGTGGACGAACATTATTGCCCGGTCTGGTGTCACCGAAACTACGAAACACGCGTTGATTGCCGTCGATCACCACCAGCGCCATGCCCGTGGCGC\n\
+\n\
AAAAAEEAEEEEEEEEE6EE/EEEEEEEEAEEAEEEEEEEEEEEEEEAEEEA/A/EEEAEEEEEE/EE</EAEEEEEE/EEE/<//AEEE<E/E/EEEEEEEEE/AAA6EAE<<EE<EEEEEA6E<EAEEAA<EEEAEAEEA<<<<EEAA\n";
    char * buf=(char *) bufstr.c_str();
    srp sr;
    FasqQualThreshold qual_thres;
    qual_thres.k=20;
    qual_thres.nucl_score_threshold=14+k_phred_32;
    qual_thres.min_correct_k_mers_in_read=78;
    T_buf_info buf_info;
    FqBaseBackend::setQualThreshold(qual_thres);
    //FqMainBackend::setTreatPEMode(treat_PE_as_single);
    FqMainBackend be(&sr);
    buf_info.buf=buf;
    buf_info.pchar=buf;
    buf_info.cnt=0;
    buf_info.real_bufsize=strlen(buf);
    be.processBuf(buf_info,f_id1,348);
    // check that read is rejected
    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
       for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
         for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
           cnt_read++;
         }
       }
    }
    assert(cnt_read==0);
}

void test_processBufSinglePlusCharBug() {
	srp::reverse_iterator rit;
	i_dim::iterator it_offs;
	k_dim::iterator it_struct;
	std::string bufstr="@SRR001666.4 071112_SLXA-EAS1_s_7:5:1:792:346/2\n\
AAAGTTAACGAACGCGCGAAAGGTCTGGAAGGTATC\n\
+\n\
IIIIIIII@+IIIIIIIIB@7IIF=III+CIID0I+\n\
@NB501291:419:HWLVNBGXK:1:11101:20148:1052 2:N:0:ATCTCAGG+NGGAGAGA\n\
GATTTTAAANNNNNNNNNAAATNNNNNNNNNNNNNNNCNNNNNNNNNNNNNNAANTTAANGNAANCTGATTACCATGNTAGNNCATATTAGTTNGANCGCCGCGTTNTTCNANACATANNGTAATCNNANTNACTAAGTATTCNTNCGGTA\n\
+\n\
AAAA/EEEE#########EEEE###############E##############EE#EEEE#E#EE#/EEEEEEEAEEE#EEE##<AAEEEEEEE#AE#6EE/EEE<E#EEE#E#EEEA/##/EEEE<##<#/#/<AE/AA/EE/#A#/AAA/\n\
@NB501291:419:HWLVNBGXK:1:11101:11957:1078 2:N:0:ATCTCAGG+CGGAGAGA\n\
ATTATATGATGCAGTAAAAGCCATTGGTGAAGAGCTATGCCCACAATTAGGTATTACTATTCCGGTGGGTAAGGACTCAATGTCCATGAAAACGCGTTGGCAAGATGATCAAGGGAAAACGAAAGAAGTTATTTCACCGCTCTCTTTAGTT\n\
+\n\
AAAAAEEEEEEEEEEEEE/EEEEEEEEEEEEEEEEEEEEE<EEEAEEEEA<EAEEEEEEEAAAAE<EEEE/EEEEAAEEEEEEE<EEEEAAEEE<AE<EE<EEEEAEE/EE/EEEE<EE/EE/E/EEEEEAE<E</<<<AE<AAAEEE/AE\n";
	char * buf=(char *) bufstr.c_str();
	srp sr;
	T_buf_info buf_info;
	FasqQualThreshold qual_thres;
	qual_thres.min_correct_k_mers_in_read=1;
	qual_thres.nucl_score_threshold=k_phred_32;
	qual_thres.k=25;
	FqBaseBackend::setQualThreshold(qual_thres);
	//FqMainBackend::setTreatPEMode(treat_PE_as_single);
	FqMainBackend be(&sr);
	buf_info.buf=buf;
	buf_info.pchar=buf;
	buf_info.cnt=0;
	buf_info.real_bufsize=strlen(buf);
	unsigned char f_id1=1;
	be.processBuf(buf_info,f_id1,855);
	int cnt_read=0;
	for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
		int score=rit->first;
	    for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
	    	unsigned int q=it_offs->first;
	    	assert(q==0);
	        for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
	            cnt_read++;
	            if (cnt_read==1) {
	            	assert(score==9); // real score is 5016+151*k_phred_32 but it is normalized by taking the quotient of a division by 1000.
	            }
	            else if (cnt_read==2) {
	            	assert(score==8); // real score is 3076 + 151*k_phred_32
	            }
	            else if (cnt_read==3) {
	            	assert(score==2); // real score is 1256 + 36*k_phred_32 (we don't do the substraction to gain calculation time).
	            }
	        }
	    }
	}
	assert(cnt_read==3);

}

void test_processBufPEWithPlusChar() {
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    unsigned char f_id1=1;
    std::string bufstr_PE1="@SRR001666.4 + @071112_SLXA-EAS1_+s_7:5:1:792:346/2\n\
AAAGTTAACGAACGCGCGAAAGGTCTGGAAGGTATC\n\
+\n\
IIIIIIII@+IIIIIIIIB@7IIF=III+CIID0I+\n\
@NB501291:419:HWLVNBGXK:1:11101:20148:1052 2:N:0:ATCTCAGG+NGGAGAGA\n\
GATTTTAAANNNNNNNNNAAATNNNNNNNNNNNNNNNCNNNNNNNNNNNNNNAANTTAANGNAANCTGATTACCATGNTAGNNCATATTAGTTNGANCGCCGCGTTNTTCNANACATANNGTAATCNNANTNACTAAGTATTCNTNCGGTA\n\
+\n\
AAAA/EEEE#########EEEE###############E##############EE#EEEE#E#EE#/EEEEEEEAEEE#EEE##<AAEEEEEEE#AE#6EE/EEE<E#EEE#E#EEEA/##/EEEE<##<#/#/<AE/AA/EE/#A#/AAA/\n\
@NB501291:419:HWLVNBGXK:1:11101:11957:1078 2:N:0:ATCTCAGG+CGGAGAGA\n\
ATTATATGATGCAGTAAAAGCCATTGGTGAAGAGCTATGCCCACAATTAGGTATTACTATTCCGGTGGGTAAGGACTCAATGTCCATGAAAACGCGTTGGCAAGATGATCAAGGGAAAACGAAAGAAGTTATTTCACCGCTCTCTTTAGTT\n\
+\n\
AAAAAEEEEEEEEEEEEE/EEEEEEEEEEEEEEEEEEEEE<EEEAEEEEA<EAEEEEEEEAAAAE<EEEE/EEEEAAEEEEEEE<EEEEAAEEE<AE<EE<EEEEAEE/EE/EEEE<EE/EE/E/EEEEEAE<E</<<<AE<AAAEEE/AE\n";
    std::string bufstr_PE2="@SRR001666.4 + @071112_SLXA-EAS1_+s_7:5:1:792:346/2\n\
AAAGTTAACGAACGCGCGAAAGGTCTGGAAGGTATC\n\
+\n\
IIIIIIII@+IIIIIIIIB@7IIF=III+CIID0I+\n\
@NB501291:419:HWLVNBGXK:1:11101:20148:1052 2:N:0:ATCTCAGG+NGGAGAGA\n\
GATTTTAAANNNNNNNNNAAATNNNNNNNNNNNNNNNCNNNNNNNNNNNNNNAANTTAANGNAANCTGATTACCATGNTAGNNCATATTAGTTNGANCGCCGCGTTNTTCNANACATANNGTAATCNNANTNACTAAGTATTCNTNCGGTA\n\
+\n\
AAAA/EEEE#########EEEE###############E##############EE#EEEE#E#EE#/EEEEEEEAEEE#EEE##<AAEEEEEEE#AE#6EE/EEE<E#EEE#E#EEEA/##/EEEE<##<#/#/<AE/AA/EE/#A#/AAA/\n\
@NB501291:419:HWLVNBGXK:1:11101:11957:1078 2:N:0:ATCTCAGG+CGGAGAGA\n\
ATTATATGATGCAGTAAAAGCCATTGGTGAAGAGCTATGCCCACAATTAGGTATTACTATTCCGGTGGGTAAGGACTCAATGTCCATGAAAACGCGTTGGCAAGATGATCAAGGGAAAACGAAAGAAGTTATTTCACCGCTCTCTTTAGTT\n\
+\n\
AAAAAEEEEEEEEEEEEE/EEEEEEEEEEEEEEEEEEEEE<EEEAEEEEA<EAEEEEEEEAAAAE<EEEE/EEEEAAEEEEEEE<EEEEAAEEE<AE<EE<EEEEAEE/EE/EEEE<EE/EE/E/EEEEEAE<E</<<<AE<AAAEEE/AE\n";
    char * buf_PE1=(char *) bufstr_PE1.c_str();
    char * buf_PE2=(char *) bufstr_PE2.c_str();
    srp sr;
    T_buf_info buf_info;
    T_buf_info PE2_buf_info;
    FasqQualThreshold qual_thres;
    qual_thres.min_correct_k_mers_in_read=1;
    qual_thres.nucl_score_threshold=k_phred_32;
    qual_thres.k=25;
    FqBaseBackend::setQualThreshold(qual_thres);
    //FqMainBackend::setTreatPEMode(treat_PE_as_single);
    FqMainBackend be(&sr);
    FqAuxBackend be2;

	PE2_buf_info.buf=buf_PE2;
	PE2_buf_info.pchar=buf_PE2;
	PE2_buf_info.cnt=0;
	PE2_buf_info.real_bufsize=strlen(buf_PE2);

	buf_info.buf=buf_PE1;
	buf_info.pchar=buf_PE1;
	buf_info.cnt=0;
	buf_info.real_bufsize=strlen(buf_PE1);

	be2.buf_info=PE2_buf_info;

	be.setAuxProcessor(&be2);

	be.processBuf(buf_info,f_id1,855);
	int cnt_read=0;
	for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
		int score=rit->first;
	    for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
	    	unsigned int q=it_offs->first;
	    	assert(q==0);
	        for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
	            cnt_read++;
	            if (cnt_read==1) {
	            	assert(score==19); // real score is 2*(5016+151*k_phred_32) but it is normalized by taking the quotient of a division by 1000.
	            }
	            else if (cnt_read==2) {
	            	assert(score==16); // real score is 2*(3076 + 151*k_phred_32)
	            }
	            else if (cnt_read==3) {
	            	assert(score==4); // real score is 2*(1256 + 36*k_phred_32) (we don't do the substraction to gain calculation time).
	            }
	        }
	    }
	}
	assert(cnt_read==3);

}

void test_processBufPE() {
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;
    unsigned char f_id1=1;
    std::string bufstr_PE1="@NS500443:65:H573HAFXX:1:11101:20964:1048/1\n\
ACATTTCACTGGTCATGAGCTTGGTGAGAGAAGCGATACGAATGACCGAATCAAGCTGTGGACGAACATTATTGCCCGGTCTGGTGTCACCGAAACTACGAAACACGCGTTGATTGCCGTCGATCACCACCAGCGCCATGCCCGTGGCGC\n\
+\n\
AAAAAEEAEEEEEEEEE6EE/EEEEEEEEAEEAEEEEEEEEEEEEEEAEEEA/A/EEEAEEEEEE/EE</EAEEEEEE/EEE/<//AEEE<E/E/EEEEEEEEE/AAA6EAE<<EE<EEEEEA6E<EAEEAA<EEEAEAEEA<<<<EEAA\n";

    std::string bufstr_PE2="@NS500443:65:H573HAFXX:1:11101:20964:1048/2\n\
CTGATATCGTTGATCGTTATGCCGAGCATATCTTTTATGGTAGCGGCGCCACGGGCATGGCGCTGGTGGTGATCGACGGCAATCAACGCGTGTTTCGTAGTTTCGGTGACACCAGACCGGGCAATAATGTTCGTCCACAGCTTGATTCGGT\n\
+\n\
AAAAAEEEEEEEEEEAEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEEEAEEEEEEEEEEEEEEAEEEEEEEEEEEEEAEEE/EEEEAE<EEEEEEEEEEAEEA<<E<EAEAAEE<EEEEEEEEEEEEEAE</A<AEEEE<EAAA<AA\n";

    char * buf_PE1=(char *) bufstr_PE1.c_str();
    char * buf_PE2=(char *) bufstr_PE2.c_str(); 
    srp sr;
    FasqQualThreshold qual_thres;
    qual_thres.k=20;
    qual_thres.nucl_score_threshold=14+k_phred_32;
    qual_thres.min_correct_k_mers_in_read=78;
    T_buf_info buf_info;
    T_buf_info PE2_buf_info;
    FqBaseBackend::setQualThreshold(qual_thres);
    //FqMainBackend::setTreatPEMode(0);

    FqMainBackend be(&sr);

    FqAuxBackend be2;


    buf_info.buf=buf_PE1;
    PE2_buf_info.buf=buf_PE2;
    buf_info.pchar=buf_PE1;
    PE2_buf_info.pchar=buf_PE2;
    buf_info.cnt=0;
    PE2_buf_info.cnt=0;
    buf_info.real_bufsize=strlen(buf_PE1);
    PE2_buf_info.real_bufsize=strlen(buf_PE2);
  
    be2.buf_info=PE2_buf_info;

    be.setAuxProcessor(&be2);

    be.processBuf(buf_info,f_id1,348);

    // check that read is selected
    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
       for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
         for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
           cnt_read++;
         }
       }
    }
    assert(cnt_read==1);
}


void Aux_MimicBigPEFilesWithMQOptions(const FasqQualThreshold& qual_thres,const int bufsize,const int nb_expected_reads) {
    srp sr;
    char * fq_1_test=(char *) "../test/data/unit/09-4607_S43_R1_big.fastq";
    char * fq_2_test=(char *) "../test/data/unit/09-4607_S43_R2_big.fastq";
    char * fq_1_test_undef=(char *) "../test/data/unit/09-4607_S43_R1.undef.fastq";
    char * fq_2_test_undef=(char *) "../test/data/unit/09-4607_S43_R2.undef.fastq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;


    FqBaseBackend::setQualThreshold(qual_thres);

    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr,bufsize);

    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
            }
        }
    }
    assert(cnt_read==nb_expected_reads);
}


void test_MimicBigPEFilesWithMQOptions() {
  int bufsize;
    FasqQualThreshold qual_thres;
    qual_thres.k=20;
    qual_thres.nucl_score_threshold=14+k_phred_32;
    qual_thres.min_correct_k_mers_in_read=100;

    bufsize=500;
    Aux_MimicBigPEFilesWithMQOptions(qual_thres,bufsize,6);
    bufsize=400;
    Aux_MimicBigPEFilesWithMQOptions(qual_thres,bufsize,6);
    bufsize=800;
    Aux_MimicBigPEFilesWithMQOptions(qual_thres,bufsize,6);

    qual_thres.min_correct_k_mers_in_read=180;
    bufsize=500;
    Aux_MimicBigPEFilesWithMQOptions(qual_thres,bufsize,3);
    bufsize=400;
    Aux_MimicBigPEFilesWithMQOptions(qual_thres,bufsize,3);
    bufsize=800;
    Aux_MimicBigPEFilesWithMQOptions(qual_thres,bufsize,3);
}

int main(int argc, char **argv) {
	cout<<"test for the case where there are + characters in read score and or read id"<<endl;
	//test_processBufSinglePlusCharBug();
	test_processBufPEWithPlusChar();
    cout<<"test for single file"<<endl;
    test_processSingleFile();
    cout<<"test for PE files"<<endl;
    test_processPEFiles();
    cout<<"test the case of records that contain @ character in quality score"<<endl;
    test_processPEfilesWithA();
    cout<<"test for both single and PE files"<<endl;
    test_processAllFiles(); // mix PE together with single; nearly as in real life.
    cout<<"testing higher level function processInputFiles"<<endl;
    test_processInputFiles();
    cout<<"test processing single files with thresholds (nucleotide quality score and minimum number of valid k_mers in read). "<<endl;
    test_processBufSingle();
    test_processSingleFileWithMQOption();
    cout<<"test processing PE files with thresholds (nucleotide quality score and minimum number of valid k_mers in read). "<<endl;
    test_processBufPE();
    test_processPEFilesWithMQOptions();
    cout<<"test processing files with thresholds (nucleotide quality score and minimum number of valid k_mers in read) mimic big files that need several read operations. "<<endl;
    test_MimicBigPEFilesWithMQOptions();
    cout<<"test processing PE files with nucleotide quality threshold."<<endl;
    test_processPE_nuclthreshold();
    cout<<"test processing PE files with lots of errors in PE2; nucleotide quality threshold"<<endl;
    test_processPE_nuclthreshold2();
    cout<<"test processing PE files; quality score threshold and minimum number of valid k-mer."<<endl;
    test_processPE_not_as_singleWithMQ();
    cout<<"done"<<endl;
}
