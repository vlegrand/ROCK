/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
 
#include <stdio.h>
#include <iostream>
#include <limits.h>
#include <assert.h>
#include <stdlib.h>
#include "srp.h"
#include "fqreader.h"

using namespace std;

void test_processSingleFile() {
    //printf("MAX_UINT=%u \n",UINT_MAX);
    srp sr;
    unsigned char f_id=1;
    processSingleFile((char *) "../data_perf_tests/LM_R2_10x.fq",f_id,&sr);
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;

    int cnt_read=0;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        unsigned long score=rit->first;
        cout << "score="<<rit->first<<endl;
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                unsigned char fid_stored=it_struct->fileid;
                cnt_read++;
                int tmp=fid_stored >>4;
                cout<<" fileid="<<tmp<<" read_a1="<<it_struct->read_a1<<endl;
            }
        }
    }
    cout<<"number of reads: "<<cnt_read;
}

void test_processPEFiles() {
    char * fq_1_test=(char *) "data/test_PE1.fq";
    char * fq_2_test=(char *) "data/test_PE2.fq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;

    srp sr;

    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr);
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;

    unsigned char masque=0x0F;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        // cout << "score="<<rit->first<<endl;
        unsigned long score=rit->first;
        if (cnt_read==0 || cnt_read==1) assert(score==10);
        if (cnt_read==2) assert(score==9);
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            assert(offset_quotient==0);
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                unsigned char fid_stored=it_struct->fileid;
                assert(fid_stored >>4==f_id1);
                assert((fid_stored &masque)==f_id2);
                if (cnt_read==0) {
                    assert(it_struct->read_a1==0);
                    assert(it_struct->read_a1==0);
                }
                if (cnt_read==1) {
                    //std::cout<<it_struct->read_a1<<" "<<it_struct->read_a2;
                    assert(it_struct->read_a1==349);
                    assert(it_struct->read_a2==349);
                }
                if (cnt_read==2) {
                    // std::cout<<it_struct->read_a1<<" "<<it_struct->read_a2;
                    assert(it_struct->read_a1==698);
                    assert(it_struct->read_a2==698);
                }
                cnt_read++;
                /*
                int tmp1=fid_stored >>4;
                int tmp2=fid_stored &masque;
                cout<<" fileid1="<<tmp1<<" read_a1="<<it_struct->read_a1<<endl;
                cout<<" fileid2="<<tmp2<<" read_a2="<<it_struct->read_a2<<endl; */
            }
        }
    }
    assert(cnt_read==3);
}

void test_processAllFiles() {
    /*char * fq_1_test=(char *) "../data_perf_tests/LM_R1_10x.fq";
    char * fq_2_test=(char *) "../data_perf_tests/LM_R2_10x.fq";*/
    
    /*char * fq_1_test=(char *) "../data_perf_tests/LM_R1_orig.fq";
    char * fq_2_test=(char *) "../data_perf_tests/LM_R2_orig.fq";*/

/*char * fq_1_test=(char *) "../data_perf_tests/LM_R1_2x.fq";
    char * fq_2_test=(char *) "../data_perf_tests/LM_R2_2x.fq";*/

char * fq_1_test=(char *) "../data_perf_tests/LM_R1_10x.fq";
    char * fq_2_test=(char *) "../data_perf_tests/LM_R2_10x.fq";

    //char * fq_single=(char *) "../data_perf_tests/LM_R2_10x.fq";

    unsigned char f_id1=1;
    unsigned char f_id2=2;
    // unsigned char f_single=3;

    srp sr;
    // size_t max_size = (size_t)-1;
    // std::cout<<"maximum of size_t is: "<<max_size<<endl;
    processPEFiles(fq_1_test, f_id1,fq_2_test, f_id2,&sr);
    
    // processSingleFile(fq_single,f_single,&sr);
/*
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int cnt_read=0;
    unsigned char masque=0x0F;

    for (rit=sr.rbegin(); rit!=sr.rend(); ++rit) { //process map in reverse order (by decreasing scores).
        unsigned long score=rit->first;
        cout << "score="<<rit->first<<endl;
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long offset_quotient=it_offs->first;
            assert(offset_quotient==0);
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                cnt_read++;
                unsigned char fid_stored=it_struct->fileid;
                int tmp1=fid_stored >>4;
                int tmp2=fid_stored &masque;
                cout<<" fileid1="<<tmp1<<" read_a1="<<it_struct->read_a1<<" fileid2="<<tmp2<<" read_a2="<<it_struct->read_a2<<endl;
            }
        }
    }
    cout<<"number of reads: "<<cnt_read;*/
}

int main(int argc, char **argv) {
    // test_processSingleFile();
    // test_processPEFiles();
    test_processAllFiles();  /* mix PE together with single; nearly as in real life.*/
}
