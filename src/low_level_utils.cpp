/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#include <string>
#include <iostream>
#include <sys/stat.h>
//#include <sys/sysctl.h>
#if !defined(_SC_PHYS_PAGES) and !__APPLE__
    #include <sys/sysinfo.h>
#elif __APPLE__
	#include <sys/sysctl.h>
#endif
#include "low_level_utils.h"

using namespace std;

/*
 * Given a filename (including full path), determine if parent directory exists.
 * If is doesn't, display error message and exit.
 */
std::string checkDirExists(const string o_file) {
    string parent_dir=".";
    std::size_t o_found = o_file.find_last_of("/");
    if (o_found!=string::npos) {
        parent_dir=o_file.substr(0,o_found);
        // cout<<parent_dir<<endl;
        struct stat info;
        if (stat(parent_dir.c_str(),&info)!=0) {
            cout<<"parent directory for output files: "<<parent_dir<<" doesn't exist."<<endl;
            exit(EXIT_FAILURE);
        }
    }
    return parent_dir;
}

/*
 * Use this function to get the amount of RAM in GB on the machine (used or not).
 * Aim is to make sure that ROCK can run on the current machine with the requirements that the user gave.
 */
unsigned long getNodePhysMemory() {
    uint64_t mem = 0;

#if defined(_SC_PHYS_PAGES) && defined(_SC_PAGESIZE)
    mem = sysconf(_SC_PHYS_PAGES);
    mem *= sysconf(_SC_PAGESIZE);
    mem /=  1 * 1024 * 1024 * 1024;
#elif __APPLE__
    size_t len = sizeof(mem);
    int ret=sysctlbyname("hw.memsize", (void*) &mem, &len, NULL,0);
    if (ret==-1) {
        cout<<"Couldn't determine how much RAM is usable on your system."<<errno<<endl;
	mem=0;
    }
    mem /=  1 * 1024 * 1024 * 1024;
#elif !defined(_SC_PHYS_PAGES) and !__APPLE__
    struct sysinfo si;
    int ret=sysinfo(&si);
    if (ret==-1) {
        cout<<"Couldn't determine how much RAM is usable on your system."<<errno<<endl;
        mem=0;
    }
    mem=si.totalram;
    mem /=  1 * 1024 * 1024 * 1024;
    //cout<<"total RAM="<<mem<<endl;
//cout<<"ROCK has not been tested on this operating system, please contact author."<<endl;
#endif

    return (unsigned long)mem;
}



