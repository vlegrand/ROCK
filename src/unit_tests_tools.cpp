/*

  Unit and non regression testing for the read_utils component.
  Keep using assert for the moment, don't want to add a dependency on boost (or any other test framework) just for the tests.

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
#include <string.h>
#include <iostream>
#include <fstream>
#include <assert.h>

using namespace std;


/*
 * Auxilliary method to compare 2 files and make ure that they are identical.
 */
int compareFilesLileByLine(char * filename1,char* filename2) {
    ifstream file1,file2;
    file1.open(filename1,ios::binary|ios::in);
    assert(file1.good());
    file2.open(filename2,ios::binary|ios::in);
    assert(file2.good());
//---------- compare number of lines in both files ------------------
    int c1,c2;
    c1 = 0; c2 = 0;
    string str;
    while(!file1.eof())
    {
        getline(file1,str);
        c1++;
    }
    file1.clear();   //  sets a new value for the error control state
    file1.seekg(0,ios::beg);
    while(!file2.eof())
    {
        getline(file2,str);
        c2++;
    }
    file2.clear();
    file2.seekg(0,ios::beg);

    if(c1 != c2)
    {
        cout << "Different number of lines in files!" << "\n";
        cout << "file1 has " << c1 << " lines and file2 has "
                     << c2 << " lines" << "\n";
        return -1;
    }
//---------- compare two files line by line ------------------
    char string1[1000], string2[1000]; // assume a read in afastq record is never longer than that.
    int j = 0;
    while(!file1.eof())
    {
        file1.getline(string1,256);
        file2.getline(string2,256);
        j++;
        if(strcmp(string1,string2) != 0)
        {
            cout << j << "-th strings are not equal" << "\n";
            cout << "   " << string1 << "\n";
            cout << "   " << string2 << "\n";
            return -1;
        }
    }

    return 0;
}



