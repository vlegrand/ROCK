/*
 * read_utils.h
 *
 *  Created on: Jan 20, 2016
 *      Author: vlegrand
 */

#ifndef READ_UTILS_H_
#define READ_UTILS_H_

#include "FqConstants.h"
#include "rock_commons.h"
#include "ReadProcessor.h"
#include "FqBaseBackend.h"

#define k_nucl_in_error 'N'

/*
 * Here, for performance matters, do not want to have to do a memmove nor strcpy or strcat to extract only DNA sequence for all
 * the fastq records that we'll have to process (potentially millions). So, use a structure to store start position of DNA sequence in buffer and its length.
 * We will not process more than 2 fastq records at a time (case of he PE reads) so it will not take too much space in memory.
 */
typedef struct {
    char fq_record_buf[MAX_FQ_RECORD_LENGTH];
    int start_idx;
    int length;
}DnaSeqStr;



void getDNASeqstr(FqBaseBackend* [],
                const rpos&,
                unsigned long,
                DnaSeqStr *,const int& nucl_qual_score_thres=0);


void init_DnaSeqStr(DnaSeqStr * dna_seq);

void decomposeReadInKMerNums(ReadProcessor& read_p, T_read_numericValues& nbrKmerDecompo,int k,DnaSeqStr a_seqs[2],int process_PE_separately=0);

#endif /* READ_UTILS_H_ */
