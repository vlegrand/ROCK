/*
 * FqBaseBackend.cpp
 *
 *  Created on: Jan 20, 2016
 *      Author: vlegrand
 */

#include <sys/stat.h>

#include <string>
#include <stdexcept>
#include <fcntl.h>
//#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>



#include "FqBaseBackend.h"

//#define DEBUG
#ifdef DEBUG
#include <cassert>
#include <iostream>
using namespace std;
#endif


T_buf_info init_buf_info(int& nread,char * buf) {
    T_buf_info buf_info;
    buf_info.real_bufsize=nread;
    buf_info.cnt=0;
    buf_info.pchar=buf;
    buf_info.buf=buf;
    buf_info.p_start_cur_rec=buf;
    return buf_info;
}

void FqBaseBackend::openInputFile(char * ficname, unsigned char id) {
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

    closeInputFile();
    i_filename=ficname;
    f_id=id;

    i_f_desc=open(i_filename,O_RDONLY,mode);
    if (i_f_desc==-1) {
        err(errno,"cannot open file: %s.",i_filename);
    }
}

void FqBaseBackend::closeInputFile() {
    if (i_f_desc!=-1) {
        close(i_f_desc);
        i_f_desc=-1;
    }
}

void FqBaseBackend::setOutputFile(char * ofilename) {
    o_filename=ofilename;
}
/*
void FqBaseBackend::setUndefFile(char * ficname) {
    undef_filename=ficname;
}*/


void FqBaseBackend::openInputFile() {
    if (i_filename==NULL || f_id==0) throw std::runtime_error("No file currently associated to this backend");
    if (i_f_desc!=-1) {
        std::string err_msg("file: ");
        err_msg+=i_filename;
        err_msg+=" is already open!";
        throw std::runtime_error(err_msg);
    }
    openInputFile(i_filename,f_id);
}


void FqBaseBackend::openOutputFile(){
    if (o_filename==NULL) throw std::runtime_error("No output file currently associated to this backend");
    openFile4Output(o_filename,&o_f_desc);
}



void FqBaseBackend::openFile4Output(char * filename, int * f_desc) {
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    if (*f_desc!=-1) {
        std::string err_msg("file: ");
        err_msg+=filename;
        err_msg+=" is already open!";
        throw std::runtime_error(err_msg);
    }
    *f_desc=open(filename,O_WRONLY|O_CREAT|O_TRUNC,mode);
    if (*f_desc==-1) {
        err(errno,"cannot open file: %s.",filename);
    }
}



void FqBaseBackend::closeOutputFile() {
    if (pos_in_w_buf!=o_buf) { // check if some fq records remains in buffer before closing file, if so, flush.
        if (write(o_f_desc, o_buf, pos_in_w_buf-o_buf)==-1) err(errno,"cannot write in file: %s.",o_filename);
        pos_in_w_buf=o_buf;
    }
    if (o_f_desc!=-1) {
        if (close(o_f_desc)==-1) err(errno,"cannot close file: %s.",o_filename);
        o_f_desc=-1;
    }
}




void FqBaseBackend::writeToOutput(const unsigned long& offset) {
    if (o_buf==NULL) {
        o_buf=(char *) malloc(FqBaseBackend::bufsize*sizeof(char));
        pos_in_w_buf=o_buf;
    }
    if (o_buf+FqBaseBackend::bufsize-pos_in_w_buf<MAX_FQ_RECORD_LENGTH) {
        // may not have enough place in buffer to store another fq record.
        // => flush buffer into output file.
        if (write(o_f_desc, o_buf, pos_in_w_buf-o_buf)==-1) err(errno,"cannot write in file: %s.",o_filename);
        pos_in_w_buf=o_buf;
    }

    int fq_rec_size=getRead(offset,pos_in_w_buf); // here aim is to avoid multiple strcpy into buffer: read directly at the right place in the buffer.
#ifdef DEBUG
    cout<<"read "<<fq_rec_size<<" char from offset: "<<offset<<" and stored them in o_buf at position: "<<pos_in_w_buf<<endl;
    cout<<"last char read: "<<*(pos_in_w_buf+fq_rec_size-1)<<endl;
    assert(*(pos_in_w_buf+fq_rec_size-1)=='\n');
    assert(*(pos_in_w_buf)=='@');
#endif
    pos_in_w_buf+=fq_rec_size;
    *pos_in_w_buf='\0';
#ifdef DEBUG
    cout<<o_buf;
#endif
}

/*
 * use C style char* rather than std::string for a matter of performance.
 * use read instead of fread for the same reason.
 *
 * Note: length of the string containing the read is returned.
 */
int FqBaseBackend::getRead(const unsigned long& offset, char * fq_record) {

    int nread,i,nb_lines,fq_rec_size;
    char * pchar;
    fq_rec_size=-1;

    if (i_f_desc==-1) throw std::runtime_error("No open file currently associated to this backend");
#ifdef DEBUG
    cout<<"going to read record at offset: "<<offset<<endl;
#endif
    off_t res=lseek(i_f_desc,offset,SEEK_SET);
    if (res==(off_t)-1) {
#ifdef DEBUG
        cout<<"Couldn't get read at offset: "<<offset<<endl;
#endif
        throw errno;
    }
    nread=read(i_f_desc,fq_record,MAX_FQ_RECORD_LENGTH);
    

#ifdef DEBUG
    assert(nread<=MAX_FQ_RECORD_LENGTH);
    assert(*(fq_record)=='@');
    cout<<"read: "<<nread<<" char from input file"<<endl;
#endif
    nb_lines=0;
    i=1;
    pchar=fq_record;
    while(i<=nread) {
        if (*pchar=='\n') {
            nb_lines++;
            if (nb_lines==4) {
                fq_rec_size=i;
                break;
            }
        }
        pchar++;
        i++;
    }
#ifdef DEBUG
    cout<<"found that fq record size is : "<<fq_rec_size<<endl;
#endif
    if (fq_rec_size==-1) errx(1,"Cannot get read!"); // here use errx rather than C++ exception because I don't want to catch an exception. If this happens, program must exit with error message and that's it.
    return fq_rec_size;
}

/* keep the end of current fastq record in memory. Used for records that are incomplete (when reading big files an fq record'start may be read in buffer 1
 whereas its end may be in buffer 2...*/
void FqBaseBackend::keepCurFastqRecord(char * buf,const int& start_rec_in_buf,const int &nread) {
    int len=nread-start_rec_in_buf;
    if (len+1>=MAX_FQ_RECORD_LENGTH) errx(1,"Buffer for current record is too small given record length.");
    memcpy(cur_fq_record,&buf[start_rec_in_buf],len);
    cur_fq_record[len]='\0';
}

void FqBaseBackend::onIncScore(T_fq_rec_info& rec_info,T_buf_info& buf_info,int& n) {
    unsigned int s=(int)*buf_info.pchar;
    // printf("s=%u; qual_thres.nucl_score_threshold=%d \n",s,qual_thres.nucl_score_threshold);
    unsigned int remaining_nucl=rec_info.nb_nucleotides_in_read-rec_info.idx_nucl_in_read;
    if ((qual_thres.nucl_score_threshold-k_phred_32) and (s<=qual_thres.nucl_score_threshold)) { // maybe TODO rewrite this with chained ternary operators once it is clear to see if it improves performances.Not useful: performance bottleneck is not here but in median calculation (42% of time approximatively for both filters).
        if (rec_info.idx_nucl_in_read<=qual_thres.k-1) { // error is found in the first k nucleotides
            int nb_k_mers=rec_info.nb_nucleotides_in_read-qual_thres.k+1;
            (nb_k_mers>rec_info.idx_nucl_in_read+1)?rec_info.nb_k_mers_in_error=rec_info.idx_nucl_in_read+1:rec_info.nb_k_mers_in_error=nb_k_mers;
        }
        else if (n>0 && remaining_nucl>n) {
            (qual_thres.k-n<remaining_nucl-n)?rec_info.nb_k_mers_in_error+=qual_thres.k-n:rec_info.nb_k_mers_in_error+=remaining_nucl-n;
        } else if (n<=0) { // no error already encountered in the previous k-1 nucleotides.
            (qual_thres.k<remaining_nucl)?rec_info.nb_k_mers_in_error+=qual_thres.k:rec_info.nb_k_mers_in_error+=remaining_nucl;
        }
        n=qual_thres.k;
    }
    n--;
    rec_info.st+=s;
    rec_info.idx_nucl_in_read++;
}

/*
void FqBaseBackend::debug_processBuf(int evt,const T_buf_info& buf_info,const unsigned long& rstart_offset) { // put all the bebug stuff in a separate method so that processBuf is shorter and more "lisible".
#ifdef DEBUG
    static int nb_start; // number of fq record start and stop found in current buffer.
    static int nb_stop;
    static int tot_nb_stop=0;
    static int tot_nb_start=0;
    static int idx_id;
    static int is_id=0;
    if (buf_info.cnt==0) {
        nb_start=0;
        nb_stop=0;
    }

    switch(evt) {
        case on_record_new:
            cout<<"nread="<<buf_info.real_bufsize<<endl;
            cout<<"cnt="<<buf_info.cnt<<endl;
            cout<<"rstart_offset="<<rstart_offset<<endl;
            is_id=1;
            idx_id=0;
            nb_start++;
            break;
        case on_line_end:
            is_id=0;
            break;
        case on_record_end:
            nb_stop++;
            cout<<"just finished processing : "<<endl;
            cout<<current_id<<endl;
            cout<<"j obttained: rstart_offset/UINT_MAX="<<rstart_offset/UINT_MAX<<endl;
            break;
        case on_store_read_id:
            if (is_id==1) {
                if (*buf_info.pchar!='/' and *buf_info.pchar !=' ') current_id[idx_id]=*buf_info.pchar;
                else current_id[idx_id]='\0';
                idx_id+=1;
            }
            break;
        case on_buf_end:
            std::cout<<" Main BE position in buffer "<<cnt<<endl;
            assert(buf_info.cnt==buf_info.real_bufsize);
            cout<<"Main BE Found "<<nb_start<<" and "<<nb_stop<<" start/stop of record in this buffer."<<endl;
            tot_nb_start+=nb_start;
            tot_nb_stop+=nb_stop;
            cout<<"Main BE Total cumulated number of start: "<<tot_nb_start<<" Total number of stop: "<<tot_nb_stop<<endl;
            break;
        case on_record_end_pe:
            cout<<p_auxFqProcessor->current_id<<endl;
            assert(strcmp(current_id,p_auxFqProcessor->current_id)==0);
            break;
    }
#endif
}*/


