/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef FQMAINBACKEND_H_
#define FQMAINBACKEND_H_

#include "FqBaseBackend.h"
#include "FqAuxBackend.h"
#include "FqConstants.h"



class FqMainBackend : public FqBaseBackend {
    // static int treat_PE_as_single;
    //static int treat_PE_separately;
    unsigned long cnt_k_mers; //! counter for the total number of k-mers found in a single file or in a pair of PE files.
                              //! This includes k-mers in error.
    FqAuxBackend * p_auxFqProcessor; /* Another fastq processor component is necessary for handling the case of PE reads.*/
    srp * p_scoreReadStruct; /* Where we store information about the reads. */
    // char current_id[50]; used only for debug
    // void debug_processBuf(int evt,const T_buf_info&, const unsigned long &);
    //void writeToUndefFile(const T_buf_info&);
    void onEndFastqRecord(T_fq_rec_info& rec_info,const T_buf_info& bufinfo);


    friend void test_processInputFiles();
public:
    
    FqMainBackend(srp*);

    void setAuxProcessor(FqAuxBackend* fq2ndProc) {
        p_auxFqProcessor=fq2ndProc;
    }

    void processFile(char * filename,unsigned char f_id);

    void processBuf(T_buf_info&,unsigned char f_id,unsigned long cur_offset);

    /*static void setTreatPEMode(const int& treat_PE){
        FqMainBackend::treat_PE_separately=treat_PE;
    }*/

    unsigned long getCntKMers() {
    	return cnt_k_mers;
    }
};

#endif /* FQMAINBACKEND_H_ */
