/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef ROCKPARAMS_H_
#define ROCKPARAMS_H_


#include <cstdlib>
#include <string>
#include <vector>
#include <getopt.h>
#include <iostream>
#include "low_level_utils.h"
#include "FqConstants.h"
#include "rock_commons.h"
#include "CountMinSketch.hpp"

#define defaultGRPMAXSize 1 //(in gigabytes) Default maximum size of srp structure but of course this size depends of the number of reads we are processing.
                            // 1 GB is enough to store approx 82 millions of reads (must take into account memory needed for stl containers).
                            // This constant is used only if you use rock's default behavior (use 90% of the machine's RAM).
                            // If you specify a lambda, it is possible to use more memory for storing more reads and less memory for the CMS.

#define k_sep_pair_end ','
#define k_ext '.'
#define path_sep '/'
#define k_proposed_lamda 4

#define k_err_parms_inconsistency 1



typedef struct {
    int nucl_score_threshold;
    int min_correct_k_mers_in_read;
    int k;
}FasqQualThreshold;


using namespace std;



class ROCKparams{
    static const int output_ext=1;
    static const int undef_ext=2;

    CMSparams parms;
    FasqQualThreshold qual_thres;
    int g; // max RAM to use for the CMS if specified by the user.
    unsigned long nb_k_mers; // expected number of k-mers in input data if specified by the user.
    int k; // size of the k-mers
    int verbose_mode;
    // int process_PE_separately;
    std::string input_file,output_file;

    std::vector<IO_fq_files> single_files;
    vector<PE_files> v_PE_files;
    int f_id;
    unsigned long cms_size;
    float expected_collision_proba;


    void processMainArgs(int optind, int argc, char ** argv,std::vector<string>& v_input_lines);
    int loadInOutFileArgs(const std::string& input_file,const std::string& output_file,std::vector<string>& v_input_lines,std::vector<string>& v_output_lines);
    int loadFileArgs(const std::string& afile,std::vector<string>& v_lines);
    void removePathfromFName(string& FName);
    void changeExtension(string& FName,const int &);
    string genUndefFilename(const string& ,const string&);
    void genOutFilenames(const std::vector<string>& v_input_lines,std::vector<string>& v_output_lines);
    int processInOutFileArgs(const std::vector<string>& v_input_lines,std::vector<string>& v_output_lines,std::vector<IO_fq_files>& single_files,vector<PE_files>& v_PE_files,int& f_id);

    /*
     * Checks options and arguments consistency (ex kappa_prime not superior to kappa; output file directory exists and so on...)
     * Also set some CMS parameters depending on value of other options.
    */
    void optArgConsistency(const string& input_file,const string& output_file,
                           const int& g,CMSparams& parms,const unsigned long& nb_k_mers,
                           const std::vector<string>& v_input_lines);

    int getBestLambdaForN(const unsigned long& nb_k_mers,const int& smallest_kappa,const unsigned int& m,const float& max_collision_proba);

    friend void test_processIOFileArgs();
    friend void test_getBestLambdaForN();


public:

    FasqQualThreshold getQualThreshold();

    ROCKparams() {
        f_id=0; // to generate id of input/output fastq files. +1=total number of files.
        g=0;
        k=25;
        nb_k_mers=0;
        parms.kappa=70;
        parms.kappa_prime=0;
        parms.lambda=0;
        parms.wanted_max_collision_proba=k_max_collision_proba;
        qual_thres.min_correct_k_mers_in_read=1;
        qual_thres.nucl_score_threshold=k_phred_32;
        qual_thres.k=k;
        verbose_mode=0;
        parms.process_PE_separately=0;
        cms_size=0;
        expected_collision_proba=0.0; //! collision probability that is computed at the beginning of ROCK from the expected number of distinct k-mers provided by the user.
        parms.max_filter_size=getNodePhysMemory()/100.0*90-defaultGRPMAXSize; // Prefer not to use all the machine's memory
        if (parms.max_filter_size==0) throw EXIT_FAILURE;
    }


    void initFromMainOptsArgs(int argc,char ** argv);



    CMSparams getCMSparams() const {
        return parms;
    }

    FasqQualThreshold getQualThresholds() {
        return qual_thres;
    }


    int get_f_id() {
        return f_id;
    }

    std::vector<IO_fq_files> get_single_files() const {
        return single_files;
    }

    vector<PE_files>  get_PE_files() const {
        return v_PE_files;
    }

    int get_k() const {
        return k;
    }


    unsigned long getFilterSize() const {
    	return cms_size;
    }


    int is_verbose() {
        return verbose_mode;
    }

    unsigned long get_expected_nbKmers() const {
    	return nb_k_mers;
    }

    float get_expected_collision_proba() const {
    	return expected_collision_proba;
    }

};


#endif /* ROCKPARAMS_H_ */
