/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
 
#include <cmath>


/*
 * logarithm of the gamma function;
 * from here: https://www.cec.uchile.cl/cinetica/pcordero/MC_libros/NumericalRecipesinC.pdf
 */
double gammln(const unsigned long& xx) {
	double x,y,tmp,ser;

	static double cof[6]={76.18009172947146,-86.50532032941677,\
			24.01409824083091,-1.231739572450155,\
			0.001208650973866179,-0.000005395239384953};
	int j;
	y=x=xx;
	tmp=x+5.5;
	tmp -=(x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++) ser+=cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}


/*
 * here x is the smallest value of the counter for a given k-mer in the CMS.
 * m is the size of a CMS array (AC: in fact, we use the biggest prime number as an approximation of the array's size).
 */
double pmf(const float&x,const unsigned long& nb_k_mers,const unsigned int& m)  {
	double res;
    double lnn=log(nb_k_mers);
    double lnm=log(m);
    double diff=lnn-lnm;
    double p=x*diff;
    diff=double(nb_k_mers)/m;
    //double truc=exp(diff);
    double tmp=p-diff;
    double g=gammln(x+1);
    tmp -=g;
    res=exp(tmp);
    return res;
}

double ccdf(const int& smallest_kappa,const unsigned long& nb_k_mers,const unsigned int& m) {
	double res;
	double s=0.0;
	for (int i=0;i<=smallest_kappa;i++) {
		s+=pmf(i,nb_k_mers,m);
	}
	res=1-s;
	if (res<0) res=0.00000001; // It happens because pmf if an approximation of the probability distribution.
			                   // So it happens that ccdf is negative but very close to 0... As we need a non null value for the next calculation,
	                           // return a very small constant.
	if (res>=0.999) res=0.999; // Need to do that because we compule ln(ccdf) to determine lamda and we need it to be non null otherwise we'd get a division by 0.
	return res;
}


float getCollisionProba(const int& smallest_kappa,const unsigned long& nb_k_mers,const unsigned int& arr_cms_size,const int& lambda) {
	float ccfdr=ccdf(smallest_kappa,nb_k_mers,arr_cms_size);
	float fpp=pow(ccfdr,lambda);
	return fpp;
}






