/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#include "FqConstants.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <stdlib.h>
#include "FqBaseBackend.h"
#include "FqAuxBackend.h"


#include <iostream>
using namespace std;

// TODO : implement some clean logger mechanism (removed debug function for that).

int FqAuxBackend::getNextRead(rinfo * p_nr) {
    int rfound=0;
    int eof=0;
    while (!rfound) {
        if (buf_info.real_bufsize==0 || (buf_info.cnt>=buf_info.real_bufsize)) {
            readBuffer();
        }
        if (buf_info.real_bufsize==0) { eof=1; break;}
        rfound=processBuffer(p_nr);
    }
    return eof;
}


int FqAuxBackend::processBuffer(rinfo * p_nr) {
    static T_fq_rec_info fq_rec_info;
    static int num_l_in_rec=5; /* counter to know on which line inside the fastq record we are */
    //static int qual_score=0;
    static int n;
    static int start_rec_in_buf;

    static unsigned long rstart_offset;
    buf_info.pchar=buf_info.buf+buf_info.cnt;
    int rfound=0;

    while (buf_info.cnt<=buf_info.real_bufsize-1 && !rfound) {
        switch (*buf_info.pchar){
            case k_read_id_start:
                if (num_l_in_rec==4) goto inc_score;
                else {
                    //debug_processBuf(on_record_new,buf_info,fq_rec_info.rstart_offset);
                    rstart_offset=cur_offset-buf_info.real_bufsize+buf_info.cnt;
                    start_rec_in_buf=buf_info.cnt;
                    buf_info.p_start_cur_rec=buf_info.pchar;
                    num_l_in_rec=1;
                    fq_rec_info.nb_nucleotides_in_read=0;
                    fq_rec_info.nb_k_mers_in_error=0;
					fq_rec_info.st=0;
					fq_rec_info.idx_nucl_in_read=0;
					n=0;
                }
                break;
            case '\n':
                // debug_processBuf(on_line_end,buf_info,fq_rec_info.rstart_offset);
                num_l_in_rec+=1;
                if (num_l_in_rec==5) { /* end of fastq record */
                    p_nr->f_id=f_id;
                    p_nr->score=fq_rec_info.st;
                    p_nr->rstart_offset=rstart_offset;
                    p_nr->nb_nucleotides_in_read=fq_rec_info.nb_nucleotides_in_read;
                    p_nr->nb_k_mers_in_error=fq_rec_info.nb_k_mers_in_error;
                    rfound=1;
                    }
                break;
            default:
            //debug_processBuf(on_store_read_id,buf_info,fq_rec_info.rstart_offset);
            (num_l_in_rec==2)?fq_rec_info.nb_nucleotides_in_read++:fq_rec_info.nb_nucleotides_in_read;
            inc_score:
                { if (num_l_in_rec==4) onIncScore(fq_rec_info,buf_info,n);
                }
        }
        buf_info.pchar++;
        buf_info.cnt++;
    }
    if (!rfound) keepCurFastqRecord(buf_info.buf,start_rec_in_buf,buf_info.real_bufsize);
    return rfound;
}


void FqAuxBackend::readBuffer() {
    size_t buffer_size;
    (test_mode)?buffer_size=test_bufsize:buffer_size=bufsize;
    if ((buf_info.real_bufsize=read(i_f_desc,buf_info.buf,buffer_size))!=0) {
        cur_offset=lseek(i_f_desc,0,SEEK_CUR);
        buf_info.cnt=0;
        buf_info.p_start_cur_rec=buf_info.buf;
        buf_info.pchar=buf_info.buf;
    }
}
/*
 * Opens file and performs the fist read operation.
 */
void FqAuxBackend::openFile(char * ficname, unsigned char id) {
    size_t buffer_size;
    (test_mode)?buffer_size=test_bufsize:buffer_size=bufsize;
    FqBaseBackend::openInputFile(ficname,id);

    buf_info.buf=(char *) malloc(buffer_size*sizeof(char));
    if (buf_info.buf==NULL) {
            err(errno,"cannot allocate memory: %lu bytes.",buffer_size);
    }
    buf_info.real_bufsize=buffer_size; // do that to force first reading in buffer.
}

void FqAuxBackend::closeFile() {
    if (i_f_desc!=-1) {
        FqBaseBackend::closeInputFile();
        free(buf_info.buf);
        buf_info.buf=NULL;
    }
}

/*
void FqAuxBackend::writeToUndefFile() {
    FqBaseBackend::writeToUndefFile(buf_info);
}
*/



void FqAuxBackend::resetCurFqRecord() {
    cur_fq_record[0]='\0';
}
