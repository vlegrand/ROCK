/*
  Gather here typedefs and structures useful for the main program and "inter-modules" data exchange.
       I divided rock into 4 "modules":
       1- fqreader (parses fastq and fills 3D array of structures representing all reads).
       2- read-utils (utility functions for DNA reads processing)
       3- CMS (fill and use).
       4- fqwriter (writes fq filtered by CMS).

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef ROCK_COMMONS_H_
#define ROCK_COMMONS_H_

#include <string>
#include <vector>


/*
 * Gather here type and constants definitions that are common to some of the 4 different modules in ROCK
 * (fqreader, read processing, CMS filling, result writing) and to the main program.
 *
 */
#define k_max_collision_proba 0.05
#define k_max_input_files 15
#define k_max_array_in_cms 50 // Theorical maximum of arrays in the count min sketch.
#define k_arr_cms_size UINT_MAX // size of 1 CMS array

// Store the prime numbers for modulo hashing in this array.
/* Use the 50 biggest prime numbers < 2^32. They have the propriety that their binary representation 9i)
 * contains at least 13 zeroes and (ii) at least 11 occurrences of '10'.
 * This disparity in the binary representation of a primary number guaranties that all bits of a numerator x
 * will be well affected during the x%p operation.
*/
const unsigned int Pi_js[k_max_array_in_cms]={4283781797,4283781461,4283738773,4283738453,4280985941,4280898901,4275393833,4275393173,\
		4275382933,4274694821,4273296553,4273285717,4273253029,4273121621,4273034581,4272772181,\
		4272771749,4272769621,4272761509,4272728741,4272630421,4272629077,4272608581,4272608533,\
		4272599701,4272598181,4272597673,4272597353,4272597349,4272597157,4272596149,4272596117,\
		4272593557,4272592549,4272592277,4272592229,4272592213,4272592021,4272589493,4272575849,\
		4272575833,4272575819,4272575701,4272575669,4272515669,4272511657,4272511637,4272510629,\
		4272510283,4272510101};

typedef struct {
    std::string in_fq_file;
    std::string out_fq_file;
    //std::string undef_fq_file;
} IO_fq_files;

typedef struct {
    IO_fq_files PE1;
    IO_fq_files PE2;
}PE_files;


typedef struct std::vector<unsigned long> readNumericValues;

typedef struct {
    readNumericValues single_or_PE_val;
    int idx_start_PE2; // In he case where PE reads are not treated as single;
                       // indicate at which index the single_or_PE_val array starts containing values for PE2 k-mers.
}T_read_numericValues;

typedef struct {
	unsigned int nb_input_reads;
	unsigned int nb_output_reads;
}T_read_counters;




#endif /* ROCK_COMMONS_H_ */
