/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 

#include <sys/stat.h>

#include <cstdio>
#include <cassert>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "rock_commons.h"
#include "FqConstants.h"
#include "srp.h"
#include "FqMainBackend.h"
#include "fqwriter.h"

#include <iostream>
using namespace std;

#define DEBUG


char expected_content_PE1 []="@SRR1222430.3 3 length=236\n\
CAAACACCTGACGCGGTTCCAGCAGGTACTCCTGCACGCCAATTTCCGGGCGGGCAGTAAAGCGCTGTTTGCAGCCCGTCTGGTGCAGGCGCCCCAGATAGCGGCCAACCCATTCCATCTGATCAAGGTTATCCGCTTCGAACTGACGACCGCCAAGGCTTGGGAAAACGGCAAAGTAGAATCCCTGATGCTGATGAAGCGTGCTGTCATTAAATAAGAGCGGCGCAGCAACGGGC\n\
+SRR1222430.3 3 length=236\n\
CCCCCFFCFFFFGGGGGGGGGGHHHHHFGHHHHHHHHGGGGGHHHHHGGGGGGGGGGHHHHHHGGGGGHHHHHHHHGGGGHGHGHHGGHGGGGGGGGHHHHHGGGGGHGGGGHHHHGHHHHHHHHHHHHHHHHHGGGGGGGGGGGGGGGGGGGFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFFFFFFFFFFFFABF\n\
@SRR1222430.2 2 length=250\n\
AGAAATTCGCCATCAGAATAAAAACCTCATATGCACATTTTCTTGTTATTGCACAGCCTGTGCCACTTTAGCGCCAGCCTCTCCGGCAATCGTGGAGAAATTAAGGAGATAGTGTAATTTATCATGTTGCTTTTGCCGTATCGTAAAGAAACCTCGAGCTTTCCTGCCAGCAGGTAGCGAGTCTGCTTCGTCACCGCAGACCGGCGCATTATCCCTTGCCGGTGTGAAACCTCATTTCATTTAAGTCAAA\n\
+SRR1222430.2 2 length=250\n\
BBBBBFFFBBBBFGGGGGGGGGHHGGHHHHHHHHGFGHHHHHHHHGHHHFFFHHFHHHHHHEHGFHHHFGFGFGGGEGGFH@@HGCGGGHHGHHGGHFAFHHHEGFHHGGHHFHFHHHHHEHHHHHHHHHHHGHHHGGGGHHGHFGGFDGFGFHGEGFCDHGGHHHHHHHGHHEHHGHGGGGHGFHHEHGHGGGGDFGGGHHGADA?DBGGGGGGFFFFBBFFFFFFFFFFFFFFFFFBFFFFFFFFF/B\n\
@SRR1222430.1 1 length=251\n\
GCCCGCGAAGCGGAGCTGGCCGCCTGCAAAGGCCGTTCGCGCTCGCTGTCGCTGGATCTGTACGCCCAGTGGCGCTGCATGGAGGACAACCACGGCAAGTGGCGCTTCACCTCGCCGACCCATACCGTGCTGGCCTTCGCCCAGGCGCTGAAAGAGCTGGCGCAGGAGGGCGGCGTCAGCGCTCGCCATCAGCGCTACCGCAACAATCAGCGCCGTCTGGTGGCAGGGATGCGCGCGCTCGGCTTCCGGCC\n\
+SRR1222430.1 1 length=251\n\
CCCCCCCCCBBCGGCGGGGG@GGGGGHHHGHBHH@GGHGGGGGGGGG@GHGHGGGHHHHHHHHHGGGGGHHHFCGGGGHHHGHHGGHHHHGGGGCCGGGGHFFGGGGGHHHHHGGGGGGCGHHHHGGGGGGGGGGGGGGGGGGAEGGFFFFFFFFFFFFFFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDFFFFFFFFFFF:DA;CCFFBBFDAFFFAFFFFFFFFA-9A>DFFFFFBFFB\n\
";


char expected_content_PE2[]="@SRR1222430.3 3 length=236\n\
GCCCGTTGCTGCGCCGCTCTTATTTAATGACAGCACGCTTCATCAGCATCAGGGATTCTACTTTGCCGTTTTCCCAAGCCTTGGCGGTCGTCAGTTCGAAGCGGATAACCTTGATCAGATGGAATGGGTTGGCCGCTATCTGGGGCGCCTGCACCAGACGGGCTGCAAACAGCGCTTTACTGCCCGCCCGGAAATTGGCGTGCAGGAGTACCTGCTGGAACCGCGTCAGGTGTTTG\n\
+SRR1222430.3 3 length=236\n\
?BBABBFBBFFFGGGGGGGGGGHHHHHHHHHHFHHHGGGGGHHHHHHHGHHHHHGHGHHHHHHHGHFEGGHGHHHHHHGGHHHHHGGGEGGGHGHHHGHHGGCGGGDHHHHHHHHGHHGHGHHHHHHFHGGGHFGGGGGHHHHEFGGCFGHHHHGGFGGGGGGGGGGGGGGGGGFFFEFFFFFFFBFFFFF=;ABFF/FA:DB;BF.9.BFFFFFFF/AFFFFFABDFFB/9BD.;\n\
@SRR1222430.2 2 length=251\n\
AGAAATTCGCCAGGGTGATCAACGTCTCATCGTCGGCGAGGGTCAGCGCCTGCGCGGGACAGTTTTCCACACAGGCCGGCCCCGCCTCGCGGCCCTGGCATAGATCGCACTTGTGCGCGCTGGCTTTCACCAGGCCTGCGGCCTGCGGCGTCACCACCACCTGCATCACCCCGAACGGGCAGGCCACCATGCAGGATTTACAGCCAATGCATTTCTCCTGACGGACCTGAACGCTGTCGCCGGACTGGGCG\n\
+SRR1222430.2 2 length=251\n\
>1>AAFFF@11AEGGFFCGGGGGFGHFH2FF1F00EEE/@AEE0FGGFGEGGHGGCGC?EEFHEFEEHDF1EECHEFE/@@/BCCCFGAC@CC@C.CEGFHFHGHFHCEC?FH;CC?CG@@?-AF.BB0BFGF?E./EF;@?;AFF@<-@@??BFF?F-:A?BF999BBBF@@?@@@F-;@B@FF-A-9FF/BFFE/F//B/BBEFBFFFFF/BFFFFFFFEB?-@=B-/BBF--:;/A-B>--;>?EFE9\n\
@SRR1222430.1 1 length=250\n\
CGATGCTGCGCTGCATCAATAAGCTGGAAGAGATCACCAGCGGCGATCTGATTGTCGATGGTCTGAAGGTCAACGACCCGAAGGTGGACGAACGTCTGATTCGTCAGGAAGCCGGGATGGTTTTCCAGCAGTTTTATCTGTTCCCGCACCTCACGGCGCTGGAAAACGTGATGTTTGGCCCGCTGCGGGTACGCGGCGCCAGCAAGCAGGCGGCGAGTGCAGGCTATCGTCGAGCAGCGGCCGGAAGCCG\n\
+SRR1222430.1 1 length=250\n\
CCCCCCFFFCCCGGGGGGGGGGHHHHHHHHGHHHHHHHHHHGGGGGGGGHHGHHHHGHGHGHHHHHHHHHFHHHGGGGGGGGGGBFFHHGGGGGGHHGGHHHGHGGGHHHHHHGGGGGGGFGHGHHHHHHHHHGHHHFHHHHHHDFGGGGHHHHHGGAGGGGGGGGGGGGGGGFGGGFFFFFFFFFFFA=@FDF-BFFFFFFFFFFFFFEDFBDFFF-:FFFFFF/BFFFEFFFFFFFFFFF;--:DEFF\n\
";



void test_write_PE() {
    srp sr;
    T_read_counters rc;
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;

    // step 1 : fill srp with fake data
    rpos rp=init_rpos(1,1114);
    update_rpos(2,1114,0,&rp);

    rpos rp2=init_rpos(1,558);
    update_rpos(2,556,0,&rp2);

    rpos rp3=init_rpos(1,0);
    update_rpos(2,0,0,&rp3);

    i_dim& ref_i_dim=sr[600];
    k_dim& ref_k_dim=ref_i_dim[0];
    ref_k_dim.push_back(rp);

    i_dim& ref_i_dim2=sr[500];
    k_dim& ref_k_dim2=ref_i_dim2[0];
    ref_k_dim2.push_back(rp2);

    i_dim& ref_i_dim3=sr[450];
    k_dim& ref_k_dim3=ref_i_dim3[0];
    ref_k_dim3.push_back(rp3);

    // step2 : create the backends
    FqBaseBackend * map_id_backend[k_max_input_files];
    FqMainBackend * be_fq1=new FqMainBackend(&sr);
    //unsigned char f_id1=1;
    FqAuxBackend * be_fq2=new FqAuxBackend();
    map_id_backend[0]=be_fq1;
    map_id_backend[1]=be_fq2;

    be_fq1->i_filename=(char *) "../test/data/unit/klebsiella_PE1.fq";
    be_fq1->f_id=1;
    be_fq2->f_id=2;
    be_fq2->i_filename=(char *) "../test/data/unit/klebsiella_PE2.fq";
    be_fq1->setOutputFile((char *) "../test/data/unit/klebsiella_PE1_filtered.fq");
    be_fq2->setOutputFile((char *) "../test/data/unit/klebsiella_PE2_filtered.fq");

    writeFilteredFastq(map_id_backend,2,rc,sr);
    assert(rc.nb_input_reads==3);
    assert(rc.nb_output_reads==3);
    
    // step 4 : re-read output files and check their content.
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    int f_pe1=open((char *) "../test/data/unit/klebsiella_PE1_filtered.fq",O_RDONLY,mode);
    assert(f_pe1!=-1);
    char* buf=(char *) malloc(FqBaseBackend::bufsize*sizeof(char)); // test files are very small, bufsize is  enough to read them entirely.
    assert(buf!=NULL);
    int nread=read(f_pe1,buf,FqBaseBackend::bufsize);
    assert(nread<FqBaseBackend::bufsize);
    // std::cout<<buf;
#ifdef DEBUG
    int i;
    for (i=0;i<nread;i++) {
        if (buf[i]!=expected_content_PE1[i]) cout<<" character at position"<<i<<" differs: "<<buf[i]<<"!="<<expected_content_PE1[i]<<endl;
    }
#endif
    assert(strcmp(buf,expected_content_PE1)==0);
    int f_pe2=open((char *) "../test/data/unit/klebsiella_PE2_filtered.fq",O_RDONLY,mode);
    assert(f_pe2!=-1);
    nread=read(f_pe2,buf,FqBaseBackend::bufsize);
    assert(nread<FqBaseBackend::bufsize);
#ifdef DEBUG
    for (i=0;i<nread;i++) {
        if (buf[i]!=expected_content_PE2[i]) cout<<" character at position"<<i<<" differs: "<<buf[i]<<"!="<<expected_content_PE1[i]<<endl;
    }
#endif
    assert(strcmp(buf,expected_content_PE2)==0);
    free(buf);


    // step5 : remove output files from disk
    
    assert(remove((char *) "../test/data/unit/klebsiella_PE1_filtered.fq")==0);
    assert(remove((char *) "../test/data/unit/klebsiella_PE2_filtered.fq")==0);

}

int main(int argc, char **argv) {
    test_write_PE();

}


