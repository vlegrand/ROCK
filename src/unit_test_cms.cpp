/*
  Here, gather the tests for ROCK's main component: the CMS.

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
#include <iostream>
#include <assert.h>
#include <climits>

using namespace std;
#include "ROCKparams.h"
#include "CountMinSketch.hpp"



void test_getBestLambdaForN() {
	ROCKparams Rparms;
    unsigned long nb_k_mer=5000000000;
    int best=Rparms.getBestLambdaForN(nb_k_mer,2,UINT_MAX,0.05);
    assert(best==2);
    nb_k_mer=200000000;
    best=Rparms.getBestLambdaForN(nb_k_mer,5,UINT_MAX,0.1);
    assert(best==1);
    nb_k_mer=600000000;
    best=Rparms.getBestLambdaForN(nb_k_mer,70,UINT_MAX,0.1);
    assert(best==1);
    nb_k_mer=2000000000;
    best=Rparms.getBestLambdaForN(nb_k_mer,10,UINT_MAX,0.1);
    assert(best==1);
    nb_k_mer=10000000000;
    best=Rparms.getBestLambdaForN(nb_k_mer,5,UINT_MAX,0.1);
    assert(best==1);
    best=Rparms.getBestLambdaForN(nb_k_mer,1,UINT_MAX,0.05);
    assert(best==8);

    nb_k_mer=50000000000;
    best=Rparms.getBestLambdaForN(nb_k_mer,5,UINT_MAX,0.1);// this returns 90
    assert(best==90);
    best=Rparms.getBestLambdaForN(nb_k_mer,50,UINT_MAX,0.1);
    assert(best==1);

    best=Rparms.getBestLambdaForN(nb_k_mer,50,UINT_MAX,0.05);
    assert(best==1);
    best=Rparms.getBestLambdaForN(nb_k_mer,10,UINT_MAX,0.05);
    assert(best==7);
}

void test_processIOFileArgs() {
    std::vector<IO_fq_files> single_files;
    vector<PE_files> v_PE_files;
    std::vector<string> v_input_lines;
    std::vector<string> v_output_lines;
    ROCKparams main_parms;
    int f_id=0;
    int ret=main_parms.loadInOutFileArgs("../test/data/unit/list_input1.txt","../test/data/unit/list_output1.txt",v_input_lines,v_output_lines);
    assert(ret==EXIT_SUCCESS);
    ret=main_parms.processInOutFileArgs(v_input_lines,v_output_lines,single_files,v_PE_files,f_id);
    assert(ret==EXIT_FAILURE);
    f_id=0;
    v_PE_files.clear();
    single_files.clear();
    v_input_lines.clear();
    v_output_lines.clear();
    ret=main_parms.loadInOutFileArgs("../test/data/unit/list_input2.txt","../test/data/unit/list_output2.txt",v_input_lines,v_output_lines);
    ret=main_parms.processInOutFileArgs(v_input_lines,v_output_lines,single_files,v_PE_files,f_id);
    assert(ret==EXIT_FAILURE);
    f_id=0;
    v_PE_files.clear();
    single_files.clear();
    v_input_lines.clear();
    v_output_lines.clear();
    ret=main_parms.loadInOutFileArgs("../test/data/unit/list_input3.txt","../test/data/unit/list_output3.txt",v_input_lines,v_output_lines);
    ret=main_parms.processInOutFileArgs(v_input_lines,v_output_lines,single_files,v_PE_files,f_id);
    assert(ret==EXIT_SUCCESS);
    assert(f_id==14);
    assert(single_files.size()==2);
    assert(v_PE_files.size()==6);
    IO_fq_files s=single_files[0];
    assert(s.in_fq_file.compare("fifi")==0);
    assert(s.out_fq_file.compare("ofifi")==0);
    PE_files s2=v_PE_files[5];
    assert(s2.PE2.in_fq_file.compare("nono")==0);
    assert(s2.PE2.out_fq_file.compare("onono")==0);

    v_output_lines.clear();
    v_input_lines.clear();
    v_PE_files.clear();
    single_files.clear();
    ret=main_parms.loadInOutFileArgs("../test/data/unit/list_input3.txt","",v_input_lines,v_output_lines);
    ret=main_parms.processInOutFileArgs(v_input_lines,v_output_lines,single_files,v_PE_files,f_id);
    s=single_files[0];
    assert(s.out_fq_file.compare("fifi.rock.fq")==0);
    s2=v_PE_files[5];
    assert(s2.PE2.out_fq_file.compare("nono.rock.fq")==0);

}


void test_ROCKparams() {
	    cout<<"test computation of the best lambda value depending on nb distinct k-mers."<<endl;
	    test_getBestLambdaForN();
	    cout<<"testing processing of IOFile arguments ( following 2 error messages are what is expected; don't worry)."<<endl;
	    test_processIOFileArgs();
	    cout<<"done"<<endl;
}


void test_CMS(int lambda,int kappa,int kappa_prime) {
    CountMinSketch<unsigned char> cms(lambda,kappa,kappa_prime);
    int i;
    cout<<"size of the CMS component: "<<sizeof(cms)<<endl;
    int num=100*lambda;
    int rej_expected=0;
    int ret;
    for (i=0;i<num;i++) {
        cms.addKMer(num);
    }
    // Now, check that our k-mer is present in the CMS.
    int min=cms.getEstimatedNbOcc(num);
    assert((min==num) || (min==num-get_mask<unsigned char>::value-1)); // addKmer doesn't check kappa.
    
    unsigned long nb_distinct_k_mers=cms.getNbDistinctKMers();
    assert(nb_distinct_k_mers==1);

    // mimic a read (as given by part 2 output).
    T_read_numericValues read_values;
    read_values.idx_start_PE2=0;
    for (i=1;i<=400;i++) {
        read_values.single_or_PE_val.push_back(i*1000-1);
    }
    ret=cms.addRead(read_values); // read should be accepted since none of the values that it contains is equal to those I inserted previously.
    assert(ret!=rej_expected);

    nb_distinct_k_mers=cms.getNbDistinctKMers();
    assert(nb_distinct_k_mers==401);

    // mimic a vector that contains a lot of k_mers already present in the CMS in its PE2 but not in its PE1 and check that it is kept.
    T_read_numericValues read_values2;
    read_values2.idx_start_PE2=200;
    for (i=1;i<=199;i++) {
        read_values2.single_or_PE_val.push_back(i*1000-1);
    }
    for (i=200;i<=400;i++) {
        read_values2.single_or_PE_val.push_back(num);
    }
    ret=cms.addRead(read_values2);
    assert(ret!=rej_expected);

    nb_distinct_k_mers=cms.getNbDistinctKMers();
    assert(nb_distinct_k_mers==401);
    
    //mimic a vector that contains in both its PE1 and PE2 parts a lot of kmers already in the CMS and check that it is rejected.
    T_read_numericValues read_values3;
    read_values3.idx_start_PE2=200;
    for (i=1;i<=160;i++) {
        read_values3.single_or_PE_val.push_back(num);
    }
    for (i=61;i<=99;i++) read_values3.single_or_PE_val.push_back(num-1);
    for (i=200;i<=400;i++) {
        read_values3.single_or_PE_val.push_back(num);
    }
    ret=cms.addRead(read_values3);
    assert(ret==rej_expected);

    nb_distinct_k_mers=cms.getNbDistinctKMers();
    assert(nb_distinct_k_mers==401);

    // test that reads with median cov<kappa_prime are rejected.
    ret=cms.isBeneathMinKappa(read_values);
    assert(ret>0);
}





/*
 * TODO: move this somewhere else.
 * I use this test because it is in the calculation of the median that we spend the most time (approx: 50%) . I want to know what in it takes the longest to see if I can improve.
 */
/*
#include <time.h>
#include <stdlib.h>
#include <err.h>
#include "Filter.hpp"
void profiling_CMS_fill() {
    ByteCountMinSketch * pByteCMS;
    CMSparams parms;
    parms.lambda=4;
    parms.filter_PE_as_single=0;
    parms.kappa=50;
    parms.kappa_prime=20;
    parms.filter_size=getNodePhysMemory()/100.0*90-1;
    pByteCMS=new ByteCountMinSketch(parms);
    // mimic SRR dataset: approx 3 million PE of 2*250 nucleotides (2*281 k-mers if k=30).
    T_read_numericValues nbrKmerDecompo;
    nbrKmerDecompo.idx_start_PE2=281;
    srand (time(NULL));
    nbrKmerDecompo.single_or_PE_val.reserve(502);

    for (int i=0; i<3000000;i++) {
        //printf("x=%d\n",x);
        for (int j=0;j<502;j++) {
            int x=rand() % 1000000 + 1;
            nbrKmerDecompo.single_or_PE_val.push_back(x);
        }
        int keep_r=pByteCMS->addRead(nbrKmerDecompo);
        nbrKmerDecompo.single_or_PE_val.clear();
    }
    delete(pByteCMS);
}
*/

int main(int argc, char **argv) {
    int lambda=2;
    int kappa=50;
    int kappa_prime=20;


    cout<<"checking that your machine has enough RAM to run test"<<endl;
    unsigned long ram=getNodePhysMemory();
    cout<<"machine has: "<<ram<<" GB of RAM"<<endl;

    if (ram<5) {
        cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
        cout<<" WARNING: skipping this test, it requires at least 4 gigabytes of RAM to run."<<endl;
        cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
        return -1;
    }
    
    cout<<"testing CMS params"<<endl;
    test_ROCKparams();
    cout<<"testing CMS with lambda="<<lambda<<endl;
    test_CMS(lambda,kappa,kappa_prime); // Finally using C arrays (maps implied storing hash keys : 4 Bytes per k_mer overhead) but each array is of size INT_MAX...
    cout<<"done"<<endl;
    
    // profiling_CMS_fill();
    return 0;
}
