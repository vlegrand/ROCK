/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
#include <iostream>
#include <cassert>
#include <climits>
#include <cmath>
#include "math_utils.h"

using namespace std;

void test_getCollisionProba() {
	float p=getCollisionProba(2,5000000000,UINT_MAX,2);
	assert(round(p*10000)==127);
	p=getCollisionProba(2,5000000000,UINT_MAX,1);
	assert(round(p*10000)==1128);
    p =getCollisionProba(1,2,UINT_MAX,3);
    assert(p==0.0);
    p =getCollisionProba(1,1000000000,UINT_MAX,1);
    assert(floor(p*1000+0.5)==23);
    p =getCollisionProba(5,1000000000,UINT_MAX,1);
    assert(floor(p*1000+0.5)==0);
    p =getCollisionProba(5,50000000000,UINT_MAX,1);
    assert(floor(p*1000+0.5)==975);
}

void test_gammln() {
    float a,tmp;
    a=gammln(1);
    tmp=exp(a);
    assert((round(tmp*10000))/10000==1);
    a=gammln(2);
    tmp=exp(a);
    assert((round(tmp*10000))/10000==1);
    a=gammln(3);
    tmp=exp(a);
    assert((round(tmp*10000))/10000==2);
    a=gammln(4);
    tmp=exp(a);
    assert((round(tmp*10000))/10000==6);
    a=gammln(5);
    tmp=exp(a);
    assert((round(tmp*10000))/10000==24);
    a=gammln(6);
    tmp=exp(a);
    float tmp2=tmp*10000;
    float tmp3=round(tmp2)/10000;
    assert(tmp3==120);
}

void test_pmf() {
    unsigned long nb_k_mers=5000000000;
    float p=pmf(0,nb_k_mers,UINT_MAX);
    assert(round(p*10000)==3122);
    p=pmf(1,nb_k_mers,UINT_MAX);
    assert(round(p*10000)==3634);
    p=pmf(2,nb_k_mers,UINT_MAX);
    assert(round(p*10000)==2115);
}

void test_ccdf() {
	unsigned long nb_k_mers=5000000000;
	double res=ccdf(2,nb_k_mers,UINT_MAX);
	assert(round(res*10000)==1128);
}


int main(int argc, char **argv) {
	cout<<"testing the gammln function"<<endl;
	test_gammln();
	cout<<"testing the pmf function"<<endl;
	test_pmf();
	cout<<"testing the ccdf function"<<endl;
	test_ccdf();
	cout<<"testing computation of collision probability."<<endl;
	test_getCollisionProba();
}


