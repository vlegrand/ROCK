/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef SRP_H_
#define SRP_H_

#include <climits>

#include <vector>
#include <map>

#define K_SCORE_NORM_FACTOR 1000

#define debug

typedef struct { /* Here store read offset in files whose ids are stored in the fileid field .*/
    unsigned char fileid;
    unsigned int read_a1;
    long read_a2;
}rpos;


typedef struct {
    unsigned int  score; // total quality score for read.
    unsigned long rstart_offset; // read offset in file
    unsigned char f_id; // identifier of the file.
    int nb_k_mers_in_error;
    int nb_nucleotides_in_read;
}rinfo;

typedef std::vector<rpos> k_dim;
typedef std::map<unsigned long,k_dim> i_dim;
typedef std::map<int,i_dim> srp;


inline rpos init_rpos(unsigned char f_id, unsigned long rstart_offset) {
    rpos rp;
    rp.fileid=f_id <<4;
    rp.read_a1=rstart_offset%UINT_MAX; // That's Ok since this remainder cannot be bigger than UINT_MAX.
#ifdef DEBUG
    cout<<"rp.read_a1="<<rp.read_a1<<endl;
#endif
    return rp;
}

/*
 * Add Paired read information to already existing rpos structure.
 */
inline void update_rpos(unsigned char f_id,unsigned long rstart_offset,unsigned long j, rpos * rp) {
    rp->fileid=rp->fileid|f_id;
    rp->read_a2=rstart_offset-UINT_MAX*j-rp->read_a1;
}


#endif /* SRP_H_ */
