/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
 
#ifndef FILTER_HPP_
#define FILTER_HPP_
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "CountMinSketch.hpp"
#include "ReadProcessor.h"
#include "read_utils.h"
#include "ROCKparams.h"


static const unsigned short ushortmask=65535;
static const unsigned char ubytemask=255;

typedef CountMinSketch<unsigned char> ByteCountMinSketch;
typedef CountMinSketch<unsigned short> ShortCountMinSketch;



// created this class to hide implementation detail (byte versys short) from main program.
class Filter {
    ByteCountMinSketch * pByteCMS;
    ShortCountMinSketch * pShortCMS;

    FasqQualThreshold qual_thres;
    int process_PE_separately;

    void getRSS();


    template <typename T> void underlyingfillCMS(FqBaseBackend* map_id_backend[],int nb_be, int k, srp* io_sr, CountMinSketch<T>* cms);
    template <typename T> void underlyinglowFilterCMS(FqBaseBackend* map_id_backend[],int nb_be,int k,srp* io_sr, CountMinSketch<T>* pcms);

public:

    Filter(const CMSparams& parms,const FasqQualThreshold& the_qual_thres) {
        pByteCMS=NULL;
        pShortCMS=NULL;
        if (parms.kappa<ubytemask) pByteCMS=new ByteCountMinSketch(parms);
        else pShortCMS=new ShortCountMinSketch(parms);
        qual_thres=the_qual_thres;
        process_PE_separately=parms.process_PE_separately;
    }

    void fillCMS(FqBaseBackend* map_id_backend[],int nb_be,int k,srp* io_sr);
    void lowFilterCMS(FqBaseBackend* map_id_backend[],int nb_be,int k, srp* io_sr);
    unsigned long getApproxNbDistinctKMers() const ;
    unsigned long getSize();
    unsigned int getNbBitsForCounters() const; // returns the size of the dada type used for the counters of the CMS.
    std::vector<unsigned long> getUnsetBuckets() const;
};

template <typename T> void Filter::underlyingfillCMS(FqBaseBackend* map_id_backend[],int nb_be, int k, srp* io_sr, CountMinSketch<T>* cms) {
    int ret;
    srp::reverse_iterator rit;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    ReadProcessor read_p(k);
    T_read_numericValues nbrKmerDecompo;

    // 1rst step, open files before fetching reads in them
    int i;
    for (i=0;i<nb_be;i++){
        map_id_backend[i]->openInputFile();
    }

    for (rit=io_sr->rbegin(); rit!=io_sr->rend(); ++rit) { //process map in reverse order (by decreasing scores).
        for (it_offs=rit->second.begin();it_offs!=rit->second.end();it_offs++) {
            unsigned long j=it_offs->first;
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                // read dna string corresponding to fast qrecord
                DnaSeqStr a_seqs[2];
                init_DnaSeqStr(&a_seqs[0]);
                init_DnaSeqStr(&a_seqs[1]);
                getDNASeqstr(map_id_backend,*it_struct,j,a_seqs,qual_thres.nucl_score_threshold);
                // decompose dna string into k-mers and turn k_mers into numbers.
                decomposeReadInKMerNums(read_p, nbrKmerDecompo,k,a_seqs,process_PE_separately);
                // insert k-mers into CMS if overall read satisfies kappa condition.
                ret=cms->addRead(nbrKmerDecompo);
                if (!ret) {
                    // read is rejected, update rpos structure accordingly.
                    it_struct->fileid=0;
                }
                nbrKmerDecompo.single_or_PE_val.clear();
            }
        }
    }
    // last step, close fq files.
    for (i=0;i<nb_be;i++){
        map_id_backend[i]->closeInputFile();
    }
}

template <typename T> void Filter::underlyinglowFilterCMS(FqBaseBackend* map_id_backend[],int nb_be,int k,srp* io_sr, CountMinSketch<T>* cms) { // possible refactor: get k from CMS.
    srp::iterator it;
    i_dim::iterator it_offs;
    k_dim::iterator it_struct;
    int ret;
    ReadProcessor read_p(k);
    T_read_numericValues nbrKmerDecompo;

    // 1rst step, open files before fetching reads in them
    int i;
    for (i=0;i<nb_be;i++){
        map_id_backend[i]->openInputFile();
    }

    for (it=io_sr->begin(); it!=io_sr->end(); ++it) {
        for (it_offs=it->second.begin();it_offs!=it->second.end();it_offs++) {
            unsigned int j=it_offs->first;
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
                // read dna string corresponding to fastq record
                if (it_struct->fileid) {
                DnaSeqStr a_seqs[2];
                init_DnaSeqStr(&a_seqs[0]);
                init_DnaSeqStr(&a_seqs[1]);
                getDNASeqstr(map_id_backend,*it_struct,j,a_seqs);
                // decompose dna string into k-mers and turn k_mers into numbers.
                decomposeReadInKMerNums(read_p, nbrKmerDecompo,k,a_seqs,process_PE_separately);
                ret=cms->isBeneathMinKappa(nbrKmerDecompo);
                if (ret) it_struct->fileid=0;
                nbrKmerDecompo.single_or_PE_val.clear();
                }
            }
        }
    }
    for (i=0;i<nb_be;i++){
        map_id_backend[i]->closeInputFile();
    }
}

void Filter::fillCMS(FqBaseBackend* map_id_backend[],int nb_be,int k,srp* io_sr) {
    if (pByteCMS!=NULL) underlyingfillCMS<unsigned char>(map_id_backend,nb_be, k, io_sr,pByteCMS);
    else underlyingfillCMS<unsigned short>(map_id_backend,nb_be, k, io_sr,pShortCMS);
    //getRSS();
}

void Filter::lowFilterCMS(FqBaseBackend* map_id_backend[],int nb_be,int k, srp* io_sr) {
    if (pByteCMS!=NULL) underlyinglowFilterCMS<unsigned char>(map_id_backend,nb_be, k, io_sr,pByteCMS);
    else underlyinglowFilterCMS<unsigned short>(map_id_backend,nb_be, k, io_sr,pShortCMS);
}

unsigned long Filter::getApproxNbDistinctKMers() const {
    if (pByteCMS!=NULL) return pByteCMS->getNbDistinctKMers();
    else return pShortCMS->getNbDistinctKMers();
}

std::vector<unsigned long> Filter::getUnsetBuckets() const {
	if (pByteCMS!=NULL) return pByteCMS->getUnsetBuckets();
	else return pShortCMS->getUnsetBuckets();
}

/*
void Filter::getRSS() {
    struct rusage usage;
    int res2=getrusage(RUSAGE_SELF,&usage);
    if (res2==-1) err(errno,"cannot get resource usage.");
    nb_bytes_CMS=usage.ru_maxrss;
}

unsigned long Filter::getSize() {
    unsigned long cms_size=nb_bytes_CMS;
    // cms_size/=8; ru_maxrss seems to be already in Bytes.
    return cms_size;
}*/

unsigned int Filter::getNbBitsForCounters() const {
	if (pByteCMS!=NULL) return sizeof(unsigned char)*8;
	else return sizeof(unsigned short)*8;
}

#endif
