/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef FQCONSTANTS_H_
#define FQCONSTANTS_H_

#define k_read_id_start '@'
#define k_read_qual_start '+'
#define k_phred_32 33

const unsigned char mask_fid=0x0F; // used to retrieve file identifier in the case of PE reads.

#define MAX_READ_LENGTH 3000 // set this arbitrary for the moment. This will likely become a parameter of the program later.

#define MAX_FQ_RECORD_LENGTH 3000

#endif /* FQCONSTANTS_H_ */
