/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <limits.h>
#include <vector>
#include <string.h>
#include <cstdlib>

#include "rock_commons.h"
#include "fqreader.h"
#include "FqMainBackend.h"



FasqQualThreshold FqBaseBackend::qual_thres;
//int FqMainBackend::treat_PE_separately;

/*
 * Processes 1 file containing single reads.
 * Used for unit testing only.
 */
void processSingleFile(char * fq_s,unsigned char f_id, srp* io_sr) {
    FqMainBackend be_fq=FqMainBackend(io_sr);
    be_fq.processFile(fq_s,f_id);
}

/* Processes 1 pair of files containing PE reads.
 * Used for unit testing only.*/
void processPEFiles(char * fq_1, unsigned char f_id1,char * fq_2, unsigned char f_id2,srp * io_sr, size_t test_bufsize) {
    FqAuxBackend be_fq2;
    FqMainBackend be_fq1(io_sr);
    if (test_bufsize) {
        be_fq2.setTestMode(test_bufsize);
        be_fq1.setTestMode(test_bufsize);
    }
    //if (fq_1_test_undef!=NULL) be_fq1.setUndefFile(fq_1_test_undef);
    //if (fq_2_test_undef!=NULL) be_fq2.setUndefFile(fq_2_test_undef);

    be_fq2.openFile(fq_2,f_id2);
    be_fq1.setAuxProcessor(&be_fq2);
    be_fq1.processFile(fq_1,f_id1);
    be_fq2.closeFile();
}


/*
 * Processes a list of single files and a list of PE files and returns an plain old C array containing
 * pointers to Fqbackend objects.
 * This function ALLOCATES MEMORY with new.
 */
unsigned long processInputFiles(const std::vector<IO_fq_files>& single_files,\
					   const vector<PE_files>& v_PE_files,FqBaseBackend * array_be[],\
					   const FasqQualThreshold& a_qual_thres, srp* io_sr,\
					   const int& PE_process_mode) {
    unsigned long tot_nb_kmers=0;
	unsigned char f_id=1;
    FqBaseBackend::setQualThreshold(a_qual_thres);
    //FqMainBackend::setTreatPEMode(PE_process_mode);
    std::vector<IO_fq_files>::const_iterator it_single;
    for (it_single=single_files.begin();it_single!=single_files.end();it_single++) {
        FqMainBackend * be_fq=new FqMainBackend(io_sr);
        //be_fq->setUndefFile((char *) it_single->undef_fq_file.c_str());
        be_fq->processFile((char *) it_single->in_fq_file.c_str(),f_id);
        be_fq->setOutputFile((char *) it_single->out_fq_file.c_str());
        array_be[f_id-1]=be_fq;
        f_id+=1;
        tot_nb_kmers+=be_fq->getCntKMers();
    }
    vector<PE_files>::const_iterator it_pe;
    for (it_pe=v_PE_files.begin();it_pe!=v_PE_files.end();it_pe++) {
        FqMainBackend * be_fq1=new FqMainBackend(io_sr);
        unsigned char f_id1=f_id;
        f_id++;
        FqAuxBackend * be_fq2=new FqAuxBackend();
        //be_fq2->setUndefFile((char *) it_pe->PE2.undef_fq_file.c_str());
        be_fq2->openFile((char *) (it_pe->PE2.in_fq_file).c_str(),(unsigned char) f_id);
        be_fq2->setOutputFile((char *) (it_pe->PE2.out_fq_file).c_str());
        be_fq1->setAuxProcessor(be_fq2);

        //be_fq1->setUndefFile((char *) it_pe->PE1.undef_fq_file.c_str());
        be_fq1->processFile((char *) (it_pe->PE1.in_fq_file).c_str(),f_id-1);
        be_fq1->setOutputFile((char *) (it_pe->PE1.out_fq_file).c_str());
        be_fq2->closeFile();
        array_be[f_id1-1]=be_fq1;
        array_be[f_id-1]=be_fq2;
        f_id++;
        tot_nb_kmers+=be_fq1->getCntKMers();
    }
    return tot_nb_kmers;
}



