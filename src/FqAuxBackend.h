/*
 * FqAuxProcessor.h
 *
 *  Created on: Jan 7, 2016
 *      Author: vlegrand
 */

#ifndef FQAUXBACKEND_H_
#define FQAUXBACKEND_H_

#include "srp.h"
#include "FqBaseBackend.h"



class FqAuxBackend:public FqBaseBackend {
	off_t cur_offset;
    T_buf_info buf_info;

    void readBuffer();
    int processBuffer(rinfo * p_nr);

    friend void test_processBufPE();
    friend void test_processBufPEWithPlusChar();

public:
    
    // char current_id[50];

    FqAuxBackend() {
        cur_offset=0;
        buf_info.real_bufsize=0;
        buf_info.buf=NULL;
        buf_info.cnt=bufsize;
    }
    void openFile(char * ficname, unsigned char id);
    void closeFile();
    int getNextRead(rinfo *);
    //void writeToUndefFile();
    void resetCurFqRecord();

};

#endif /* FQAUXBACKEND_H_ */
