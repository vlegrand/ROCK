/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
#include <map>
#include "srp.h"
#include "FqBaseBackend.h"

//#define DEBUG
#ifdef DEBUG
#include <iostream>
using namespace std;
#endif


// TODO this looks a little bit like getDnaStr in read_utils.cpp. Some refactoring could be useful. Think about
// classes to encapsulate that without decreasing perfs.
void writeFilteredFastq(FqBaseBackend* map_id_backend[],int nb_be,T_read_counters& rc, const srp& io_sr){
    srp::const_reverse_iterator it;
    i_dim::const_iterator it_offs;
    k_dim::const_iterator it_struct;

    unsigned char f_id1;
    unsigned char f_id2;
    unsigned char fid_stored;

    // 1rst step, open files before fetching/writing reads from/to them
    int i;
    rc.nb_input_reads=0;
    rc.nb_output_reads=0;
    for (i=0;i<nb_be;i++){
         map_id_backend[i]->openInputFile();
         map_id_backend[i]->openOutputFile();
    }

    for (it=io_sr.rbegin(); it!=io_sr.rend(); ++it) {
        for (it_offs=it->second.begin();it_offs!=it->second.end();it_offs++) {
            unsigned long j=it_offs->first;
            for (it_struct=it_offs->second.begin();it_struct!=it_offs->second.end();it_struct++) {
            	rc.nb_input_reads++;
                if (it_struct->fileid) {
                	rc.nb_output_reads++;
                    fid_stored=it_struct->fileid;
                    f_id1=fid_stored >>4;
                    f_id2=fid_stored &mask_fid;
                    unsigned long offset1=j*UINT_MAX+it_struct->read_a1;
#ifdef DEBUG
     cout<<"going to output record with: j="<<j<<"read_a1="<<it_struct->read_a1<<endl;
     cout<<"j*UINT_MAX+it_struct->read_a1 = "<<j*UINT_MAX+it_struct->read_a1<<endl;
     cout<<"offset1="<<offset1<<endl;
     cout<<"f_id1-1="<<f_id1-1<<endl;
#endif
                    FqBaseBackend * be=map_id_backend[f_id1-1];
                    be->writeToOutput(offset1);
                    if (f_id2!=0) { // case of PE reads.
                        unsigned long offset2=it_struct->read_a2+offset1;
#ifdef DEBUG
    cout<<"f_id2-1="<<f_id2-1<<endl;
    cout<<"offset2="<<offset2<<endl;
#endif
                        map_id_backend[f_id2-1]->writeToOutput(offset2);
                    }
                }
            }
        }
    }

    // last step, close files.
    for (i=0;i<nb_be;i++){
         map_id_backend[i]->closeInputFile();
         map_id_backend[i]->closeOutputFile();
    }
}




