/*
  Utility methods for processing DNA reads : decomposition in overlapping k-mers, converting a k-mer into a number,
         reverse complement and so on...

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */
 
 
#ifndef READPROCESSOR_H_
#define READPROCESSOR_H_

#include <map>
#include <vector>
#include <math.h>


#include <iostream>
#include "rock_commons.h"
using namespace std;





class ReadProcessor {

    int k;
    int n;

    std::map<char, char> nucleo_rev_cpl;
    unsigned long mask_kMer;


    void init_nucleo_rev_cpl() {
        nucleo_rev_cpl['A']='T';
        nucleo_rev_cpl['T']='A';
        nucleo_rev_cpl['C']='G';
        nucleo_rev_cpl['G']='C';
        nucleo_rev_cpl['N']='N';
    }

    void init_mask(int k){
        int nb_bits=2*k;
        mask_kMer=pow(2,nb_bits);
        mask_kMer=mask_kMer-1;
    }

    friend void testDNAToNumberSimple();

    unsigned long nucleoToNumber(char s) {
        unsigned long nbr;
        switch(s)
        {
            case 'A':
                nbr=0;
                break;
            case 'C':
                nbr=1;
                break;
            case 'G':
                nbr=2;
                break;
            case 'T':
                nbr=3;
                break;
            case 'N':
                nbr=0;
                break;
            default:
                cout<<"found unexpected character in DNA string: "<<s<<endl;
                throw -1;
        }
        return nbr;
    }

    unsigned long nucleoToNumberReverse(char s,int i) {
        unsigned long nbr=0;
        char cpl=nucleo_rev_cpl[s];
        nbr=nucleoToNumber(cpl);
        nbr=nbr<<2*(i-1);
        return nbr;
    }



public:
    ReadProcessor(int k=3) {
        n=0;
        set_k(k);
        init_nucleo_rev_cpl();
    }
    void set_k(int k) {
        this->k=k;
        init_mask(k);
    }

    //! Gather here the numbers corresponding to a kmer and it's reverse complement once they were hashed.
	typedef struct {
		unsigned long nbr;
		unsigned long nbr_rev;
		unsigned int first; //! set this to 1 before processing the 1rst kmer of the read.
	} KmRevNumbers;

	void initNumbers(KmRevNumbers& numbers) {
		numbers.first=1;
		numbers.nbr=0;
		numbers.nbr_rev=0;
	}

    // Do not inline this; it is slightly slower.
    // These functions are no more used. Now using the same loop to convert hash kmer and reverse complement.
    /*unsigned long kMerToNumberReverse(char * k_m,unsigned long * p_prev);
    unsigned long kMerToNumber(char * k_m,unsigned long * p_prev);*/
    //! The 2 previous methods are now replacs by the one below.
    void kMerAndRevToNumber(char * k_m,KmRevNumbers& numbers);
    unsigned long nucleoToNumberWrap(char s);

    void getKMerNumbers(char * dnaStr,int l,readNumericValues&);


};

#endif
