/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef COUNTMINSKETCH_HPP_
#define COUNTMINSKETCH_HPP_


#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <vector>
#include <cmath>
#include "rock_commons.h"


#define BAD_TYPE -1


typedef struct {
    int lambda;
    int kappa;
    int kappa_prime;
    int max_filter_size; // max amount of RAM wanted for the CMS.
    float wanted_max_collision_proba;
    int process_PE_separately;
} CMSparams;

template <typename T>
struct get_mask {
    static const int value=BAD_TYPE;
};

template<>
struct get_mask<unsigned char> {
    static const unsigned char value=255;
};

template<>
struct get_mask<unsigned short> {
    static const unsigned short value=65535;
};


template<typename T> class CountMinSketch {

private:
    static const unsigned long mask1=1; // used only for hash64to32bs
    static const unsigned long mask2=2095103;
    static const unsigned long mask3=1023;

    int lambda;
    int kappa;
    int kappa_prime;

    T ** cms_lambda_array;
    T mask;
    std::vector<unsigned long> zeroes; //! Number of unset buckets per hash.

    inline unsigned int hash64to32(const unsigned long& w ,const int& j) {
        return w % Pi_js[j];
    }




    inline int isRCovBelowThres(const T_read_numericValues& read_val, const int& threshold);

    inline int isRCovBelowThresPartial(const readNumericValues& read_val, const int& threshold, const int& start=0,const int& stop=0);

    void init(int glambda,int gkappa,int gkappa_prime);

    // for unit tests.
    friend void test_CMS(int lambda,int kappa,int kappa_prime);


public:

    CountMinSketch(int glambda,int gkappa,int gkappa_prime) {
        init(glambda,gkappa,gkappa_prime);
    }

    CountMinSketch(CMSparams parms) {
        init(parms.lambda,parms.kappa,parms.kappa_prime);
    }

    ~CountMinSketch() {
        int j;
        for (j=0;j<lambda;j++) {
            free(cms_lambda_array[j]);
        }
        free(cms_lambda_array);
    }

    //! This method must be called after getNbDistinctKMers because getNbDistinctKmers sets the counters.
    std::vector<unsigned long> getUnsetBuckets() {
    	return zeroes;
    }


    inline void addKMer(const unsigned long& val1) {
		int j;
		unsigned int h;
		T cnt;
		j=lambda;
		while(--j>=0) {
			h=hash64to32(val1,j);
			cnt=cms_lambda_array[j] [h];
			cnt++;
			cms_lambda_array[j] [h]=(cnt & mask);
		}
	}

    // keep that just for unit testing purpose
    T getEstimatedNbOcc(const unsigned long& val);

    int addRead(const T_read_numericValues& read_val);

    int isBeneathMinKappa(const T_read_numericValues&read_val);

    unsigned long getNbDistinctKMers();

};




/*
 * Determines whether median of k-mer coverage is below threshold or not.
 */
// this is a few seconds faster
template<typename T> inline int CountMinSketch<T>::isRCovBelowThres(const T_read_numericValues& read_val, const int& threshold) {
    int PE1_below_thres;
    int PE2_below_thres=0;
    int a1=0,a2=0;
    int b1,b2=0;
    int j;
    unsigned int h;
    T min;

    int num=read_val.single_or_PE_val.size();

    read_val.idx_start_PE2?b1=read_val.idx_start_PE2:b1=num;
    read_val.idx_start_PE2?b2=num-b1:b2;

    // this proves faster than using iterators on these data (short)
    long const * p_start=(long const * )&(read_val.single_or_PE_val[0]); // TODO: when we'll use C++11: use the data() method of the vector class to access the underlying C array.
    int nb=0;
    while(nb<b1 && 2*a1<=b1) {
        j=lambda;
        min=mask;
        while (--j>=0 && min>threshold) {
            //std::cout<<*(p_start+nb)<<std::endl;
            //std::cout<<j<<std::endl;
            h=hash64to32(*(p_start+nb),j);
            //std::cout<<h<<std::endl;
            //unsigned int test=hash64to32(657922856560023,0);
            
            min=cms_lambda_array[j] [h];
        }
        (min<threshold)? a1+=1:a1;
        nb++;
    }
    PE1_below_thres=2*a1>b1;
    if (b2) {
        nb=b1;
        while(nb<num && 2*a2<=b2) {
            j=lambda;
            min=mask;
            while (--j>=0 && min>threshold) {
                h=hash64to32(*(p_start+nb),j);
                min=cms_lambda_array[j] [h];
            }
            (min<threshold)? a2+=1:a2;
            nb++;
        }
        PE2_below_thres=2*a2>b2;
        return (PE1_below_thres || PE2_below_thres);
    }
    return PE1_below_thres;
}

// C++11 style. It is equivalent to the C style optim in terms of performance.
/*
template<typename T> inline int CountMinSketch<T>::isRCovBelowThres(const T_read_numericValues& read_val, const int& threshold) {
    int PE1_below_thres;
    int PE2_below_thres=0;
    int a1=0,a2=0;
    int b1,b2=0;
    int j,h;
    T min;

    int num=read_val.single_or_PE_val.size();

    read_val.idx_start_PE2?b1=read_val.idx_start_PE2:b1=num;
    read_val.idx_start_PE2?b2=num-b1:b2;

    // this proves faster than using iterators on these data (short)
    // long const * p_start=(long const * )&(read_val.single_or_PE_val[0]); // TODO: when we'll use C++11: use the data() method of the vector class to access the underlying C array.

    long const * ptr = (num > 0) ? (long const * ) read_val.single_or_PE_val.data() : nullptr;

    int nb=0;
    while(nb<b1 && 2*a1<=b1) {
        j=lambda;
        min=mask;
        while (--j>=0 && min>threshold) {
            // printf("nb=%d k_mer=%ld\n",nb,*(p_start+nb));
            h=hash64to32(ptr[nb],j);
            min=cms_lambda_array[j] [h];
        }
        (min<threshold)? a1+=1:a1;
        nb++;
    }
    PE1_below_thres=2*a1>b1;
    if (b2) {
        nb=b1;
        while(nb<num && 2*a2<=b2) {
            j=lambda;
            min=mask;
            while (--j>=0 && min>threshold) {
                h=hash64to32(ptr[nb],j);
                min=cms_lambda_array[j] [h];
            }
            (min<threshold)? a2+=1:a2;
            nb++;
        }
        PE2_below_thres=2*a2>b2;
        if (threshold==kappa) return (PE1_below_thres || PE2_below_thres);
        else return(PE1_below_thres && PE2_below_thres);
    }
    return PE1_below_thres;
}*/



template<typename T> void CountMinSketch<T>::init(int glambda,int gkappa,int gkappa_prime) {
    lambda=glambda;
    kappa=gkappa;
    kappa_prime=gkappa_prime;
    int j;
    mask=get_mask<T>::value;
    cms_lambda_array= (T**) malloc(lambda*sizeof(T*));
    for (j=0;j<lambda;j++) {
        cms_lambda_array[j]=(T *) malloc(sizeof(T)*k_arr_cms_size);
        memset(cms_lambda_array[j],0,k_arr_cms_size);
    }
}

// keep that just for unit testing purpose
template<typename T> T CountMinSketch<T>::getEstimatedNbOcc(const unsigned long& val) {
    int j,h;
    T min=mask;
    j=lambda;
    while(--j>=0 && min>kappa) {
        h=hash64to32(val,j);
        min=cms_lambda_array[j] [h];
    }
    return min;
}

template<typename T> int CountMinSketch<T>::addRead(const T_read_numericValues& read_val) {
    int keep_r=isRCovBelowThres(read_val,kappa);
    if (keep_r) {
        readNumericValues::const_iterator it;
        // according to the intel profiler, we spend a lot of time in this loop. Try to use something slightly faster.
        long const * p=(long const *) &read_val.single_or_PE_val[0];
        int cnt;
        int stop=read_val.single_or_PE_val.size();
        for (cnt=0;cnt<stop;cnt++) {
            this->addKMer(*p);
            p=p+1;
        }
    }
    return keep_r;
}

template<typename T> int CountMinSketch<T>::isBeneathMinKappa(const T_read_numericValues& read_val) {
    int res=isRCovBelowThres(read_val,kappa_prime);
    return res;
}



/*
 * Go through the arrays of the CMS and returns an estimation of the number of distinct k_mers (new formula from Alexis)
 */
template<typename T> unsigned long CountMinSketch<T>::getNbDistinctKMers() {
    int j;
    unsigned long max=0;
    unsigned int h;
    unsigned long n;
    unsigned long m;

    for (j=lambda-1;j>=0;--j) {
        m=Pi_js[j];
        unsigned long z=0; // number of zeroes in a CMS array.
        for (h=Pi_js[j]-1;h>0;--h) { // Have to process the case of h=0 separately otherwise as h is now an unsigned int, it is always >=0 which causes an infinite loop.
            (cms_lambda_array[j] [h]==0)?  z+=1: z;
        }
        (cms_lambda_array[j] [0]==0)?  z+=1: z;
        zeroes.push_back(z);
        double lnz=log(z);
        double lnm=log(m);
        double lnm1=log(m-1);
        double deno=lnm1-lnm;
        double nume=lnz-lnm;
        n=nume/deno;
        (n>max)?max=n:max;
    }
    return max;
}

#endif /* COUNTMINSKETCH_HPP_ */
